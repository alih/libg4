/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "g4-tiff.h"

static void g4_to_pbm(G4Tiff *g4, FILE *out, int page)
{
    // XXX ignoring page for now
    (void) page;

    int w = g4_width(g4);
    int h = g4_height(g4);

    if (w <= 0 || h <= 0)
    {
        fprintf(stderr, "Invalid image size (error: %s)\n", g4_error(g4));
        exit(EXIT_FAILURE);
    }

    int bufsize = (w + 7) >> 3;
    unsigned char buf[bufsize];

    fprintf(out, "P4\n%d %d\n", w, h);
    G4Reader *r = g4_get_reader(g4);
    for (int i = 0; i < h; i++)
    {
        if (!g4_read_line(r, buf))
        {
            fprintf(stderr, "Error: %s\n", g4_reader_error(r));
            break;
        }
        fwrite(buf, 1, bufsize, out);
    }
    g4_reader_free(r);
}

static void run_g4_to_pbm(const char *input_path, const char *output_path, int page)
{
    G4Tiff *g4 = g4_open(input_path, "r");
    if (!g4)
    {
        perror(input_path);
        exit(EXIT_FAILURE);
    }

    FILE *f = strcmp(output_path, "-") ? fopen(output_path, "wb")
                                       : stdout;
    if (!f)
    {
        perror(output_path);
        g4_close(g4);
        exit(EXIT_FAILURE);
    }

    g4_to_pbm(g4, f, page);

    if (f != stdout)
        fclose(f);
    g4_close(g4);
}

int main(int argc, const char *const *argv)
{
    if (argc < 2 || argc > 4 
        || !strcmp(argv[1], "-h")
        || !strcmp(argv[1], "--help"))
    {
        fprintf(stderr, "%s - convert G4 TIFF images to PBM\n\n", argv[0]);
        fprintf(stderr, "Usage:\n");
        fprintf(stderr, "    %s input.tif [output.pbm]\n", argv[0]);
        fprintf(stderr, "\nOutput, if not specified, defaults to stdout.\n");
        //fprintf(stderr, "Page numbering starts from 1.\n");
        //fprintf(stderr, "The page number is currently ignored.\n");
        // fprintf(stderr, "The default behavior is to convert all pages.\n");
        // fprintf(stderr, "If you want a specific page on stdout, use `-':\n");
        // fprintf(stderr, "    %s input.tif - page\n", argv[0]);
        // fprintf(stderr, "TIFF files, however, can't be read from stdin.\n");
        exit(EXIT_FAILURE);
    }

    const char *input_path = argv[1];
    const char *output_path = argc > 2 ? argv[2] : "-";
    int page = 0; // 0 means all
    if (argc > 3)
    {
        page = atoi(argv[3]);
        if (page <= 0)
        {
            fprintf(stderr, "Invalid page number: %s\n", argv[3]);
            exit(EXIT_FAILURE);
        }
    }
    
    run_g4_to_pbm(input_path, output_path, page);

    return EXIT_SUCCESS;
}
