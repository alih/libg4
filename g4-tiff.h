/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef G4_TIFF_H
#define G4_TIFF_H

#include <stdbool.h>

#include "g4-reader.h"
#include "g4-writer.h"

typedef struct G4Tiff G4Tiff;

G4Tiff *g4_open(const char *path, const char *mode);
void g4_close(G4Tiff *);

/**
 * There's at most one reader in existence.
 */
G4Reader *g4_get_reader(G4Tiff *);
G4Writer *g4_get_writer(G4Tiff *);

#if 0 // not supporting that for now
/**
 * Proceed to the next image within a multi-image TIFF file.
 * There's no way to go back. Hey, this is not libtiff, OK?
 */
bool g4_next(G4Tiff *);
bool g4_has_next(G4Tiff *);
int g4_page_no(G4Tiff *);
bool g4_go_to_page(G4Tiff *, int page_no);
#endif

const char *g4_error(G4Tiff *g4);

int g4_width(G4Tiff *);
int g4_height(G4Tiff *);
void g4_set_size(G4Tiff *, int width, int height);

int g4_strip_height(G4Tiff *);
int g4_strip_count(G4Tiff *);
int g4_strip_offset(G4Tiff *, int strip_index);
int g4_strip_length(G4Tiff *, int strip_index);

void g4_dump(G4Tiff *);

 
#endif
