/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef G4_CODETABLE_H
#define G4_CODETABLE_H

typedef struct
{
    int terminal;
    const char *code;
} G4TableEntry;

extern G4TableEntry main_codes[];
extern G4TableEntry small_white_codes[];
extern G4TableEntry small_black_codes[];
extern G4TableEntry big_white_codes[];
extern G4TableEntry big_black_codes[];
extern G4TableEntry huge_codes[];

extern const int main_codes_length;
extern const int small_white_codes_length;
extern const int small_black_codes_length;
extern const int big_white_codes_length;
extern const int big_black_codes_length;
extern const int huge_codes_length;

#endif
