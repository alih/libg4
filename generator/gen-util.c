/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>

#include "gen-util.h"

void print_int_array(int *values, int length)
{
    enum { WRAP = 10 };
    for (int i = 0; i < length; i++)
    {
        if (i)
            printf(",");
        if (i % WRAP == 0)
            printf("\n    ");
        else
            printf(" ");
        printf("%d", values[i]);
    }
}

int code_value(const char *code)
{
    char *endptr;
    long i = strtol(code, &endptr, 2);
    if (*endptr || !*code)
    {
        fprintf(stderr, "Invalid code: %s\n", code);
        exit(EXIT_FAILURE);
    }
    return (int) i;
}
