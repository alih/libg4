/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "g4-constants.h"
#include "codetable.h"
#include "gen-util.h"

// This name table corresponds to g4-constants.h.
struct
{
    int terminal;
    const char *name;
} name_table[] = {
    { G4_PASS, "PASS" },
    { G4_HORIZONTAL, "HORIZONTAL" },
    { G4_EXTENSION, "EXTENSION" },
    { G4_EOFB, "EOFB" }
};
enum { N_NAMES = sizeof(name_table) / sizeof(*name_table) };

static const char *find_main_code(int terminal)
{
    for (int i = 0; i < main_codes_length; i++)
    {
        if (main_codes[i].terminal == terminal)
            return main_codes[i].code;
    }
    return NULL;
}

static void generate_main_table(bool header)
{
    if (G4_MAX_V - G4_MIN_V + 1 + N_NAMES != main_codes_length)
    {
        fprintf(stderr, "Main codetable length mismatch\n");
        exit(EXIT_FAILURE);
    }

    if (header)
    {
        puts("extern int g4_v_codetable_values[];");
        puts("extern int g4_v_codetable_lengths[];");
        return;
    }

    printf("int g4_v_codetable_values[] = {");
    int n = G4_MAX_V - G4_MIN_V + 1;
    int buf[n];
    for (int i = G4_MIN_V; i <= G4_MAX_V; i++)
        buf[i - G4_MIN_V] = code_value(find_main_code(i));
    print_int_array(buf, n);
    puts("\n};");

    printf("int g4_v_codetable_lengths[] = {");
    for (int i = G4_MIN_V; i <= G4_MAX_V; i++)
        buf[i - G4_MIN_V] = strlen(find_main_code(i));
    print_int_array(buf, n);
    puts("\n};");
}

static void generate_small_table(const char *color, G4TableEntry *table, int length,
                          bool header)
{
    // check the table
    if (length != G4_MAX_SMALL_VALUE + 1)
    {
        fprintf(stderr, "Length of the small table: expected %d, got %d\n",
                        G4_MAX_SMALL_VALUE + 1, length);
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < length; i++)
    {
        if (table[i].terminal != i)
        {
            fprintf(stderr, "In the small table: expected %d, got %d",
                            i, table[i].terminal);
            exit(EXIT_FAILURE);
        }
    }
    if (header)
    {
        printf("extern int g4_small_%s_code_values[];\n", color);
        printf("extern int g4_small_%s_code_lengths[];\n", color);
        return;
    }
    printf("int g4_small_%s_code_values[] = {", color);
    int buf[length];
    for (int i = 0; i < length; i++)
        buf[i] = code_value(table[i].code);
    print_int_array(buf, length);
    printf("};\n");
    printf("int g4_small_%s_code_lengths[] = {", color);
    for (int i = 0; i < length; i++)
        buf[i] = strlen(table[i].code);
    print_int_array(buf, length);
    printf("};\n");
}

static void generate_big_table(const char *color, G4TableEntry *table, int length,
                               G4TableEntry *huge_table, int huge_length, bool header)
{
    // check the table
    int expected_length = G4_MAX_TABLE_RUN / G4_BIG_TABLE_STEP;
    if (length + huge_length != expected_length)
    {
        fprintf(stderr, "Length of the big+huge table: expected %d, got %d\n",
                         expected_length, length + huge_length);
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < length + huge_length; i++)
    {
        int terminal = i < length ? table[i].terminal
                                  : huge_table[i - length].terminal;
        if (terminal != (i + 1) * G4_BIG_TABLE_STEP)
        {
            fprintf(stderr, "In the big table: expected %d, got %d\n",
                            i, terminal);
            exit(EXIT_FAILURE);
        }
    }
    if (header)
    {
        printf("extern int g4_big_%s_code_values[];\n", color);
        printf("extern int g4_big_%s_code_lengths[];\n", color);
        return;
    }
    puts("// We insert 0 at the beginning of these tables");
    puts("// so that they can be indexed with simply i / G4_BIG_TABLE_STEP");
    printf("int g4_big_%s_code_values[] = {", color);
    int buf[1 + length + huge_length];
    buf[0] = 0;
    for (int i = 0; i < length; i++)
        buf[i + 1] = code_value(table[i].code);
    for (int i = 0; i < huge_length; i++)
        buf[length + i + 1] = code_value(huge_table[i].code);
    print_int_array(buf, 1 + length + huge_length);
    printf("};\n");
    printf("int g4_big_%s_code_lengths[] = {", color);
    for (int i = 0; i < length; i++)
        buf[i + 1] = strlen(table[i].code);
    for (int i = 0; i < huge_length; i++)
        buf[length + i + 1] = strlen(huge_table[i].code);
    print_int_array(buf, 1 + length + huge_length);
    printf("};\n");
}

static void generate_tables(bool header)
{
    puts("");
    generate_main_table(header);
    puts("");
    generate_small_table("white", small_white_codes, 
                                  small_white_codes_length, header);
    puts("");
    generate_small_table("black", small_black_codes,
                                  small_black_codes_length, header);
    puts("");
    generate_big_table("white", big_white_codes, big_white_codes_length,
                        huge_codes, huge_codes_length, header);
    puts("");
    generate_big_table("black", big_black_codes, big_black_codes_length,
                        huge_codes, huge_codes_length, header);
}

static void h_named_writers()
{
    puts("// Here is a write macro for every named code.");
    for(int i = 0; i < N_NAMES; i++)
    {
        int terminal = name_table[i].terminal;
        const char *name = name_table[i].name;
        const char *code = find_main_code(terminal);
        printf("#define g4_bitstream_write_%s(E) \\\n", name);
        printf("    g4_bitstream_write((E), /* val: */ %d, \\\n", 
            code_value(code));
        printf("                            /* len: */ %d)\n",
            (int) strlen(code));
    } 
}

static void h_v_writer()
{
    puts("// For writing V mode codes we provide CODE/CODE_LEN pair instead.");
    puts("#define G4_V_CODE(V) (g4_v_codetable_values[(V) - G4_MIN_V])");
    puts("#define G4_V_CODE_LEN(V) (g4_v_codetable_lengths[(V) - G4_MIN_V])");
}

static void generate_h()
{
    puts("#ifndef G4_ETABLES_H");
    puts("#define G4_ETABLES_H");
    puts("");
    puts("#include \"g4-constants.h\"");
    puts("");
    h_named_writers();
    puts("");
    h_v_writer();
    generate_tables(true);
    puts("");
    puts("#endif");
}

int main(int argc, const char *const *argv)
{
    if (argc != 2 || (strcmp(argv[1], "c") && strcmp(argv[1], "h")))
    {
        fprintf(stderr, "%s - generate Huffman tables for G4 encoding\n", argv[0]);
        fprintf(stderr, "Usage:\n");
        fprintf(stderr, "    %s c >g4-etables.c\n", argv[0]);
        fprintf(stderr, "    %s h >g4-etables.h\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    printf("// DO NOT EDIT - this file was generated by gen-etables\n");

    if (!strcmp(argv[1], "c"))
        generate_tables(false);
    else
        generate_h();

    return EXIT_SUCCESS;
}
