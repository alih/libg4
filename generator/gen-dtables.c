/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "g4-constants.h"
#include "codetable.h"
#include "gen-util.h"

const char ESCAPE_EOFB[] = "0000000";
const char ESCAPE_BLACK_LONG[] = "0000001";
const char ESCAPE_HUGE[] = "00000001";

typedef struct
{
    short terminal;
    short length;
} TableEntry;

TableEntry main_table[1 << G4_MAIN_TABLE_LOG_SIZE];
TableEntry white_table[1 << G4_WHITE_TABLE_LOG_SIZE];
TableEntry black_table[1 << G4_BLACK_TABLE_LOG_SIZE];
TableEntry black_long_table[1 << G4_BLACK_LONG_TABLE_LOG_SIZE];
TableEntry huge_run_table[1 << G4_HUGE_RUN_TABLE_LOG_SIZE];
TableEntry *all_tables[] = {main_table, 
                            white_table, 
                            black_table,
                            black_long_table, 
                            huge_run_table};
int all_table_log_sizes[] = {G4_MAIN_TABLE_LOG_SIZE,
                             G4_WHITE_TABLE_LOG_SIZE,
                             G4_BLACK_TABLE_LOG_SIZE,
                             G4_BLACK_LONG_TABLE_LOG_SIZE,
                             G4_HUGE_RUN_TABLE_LOG_SIZE};
int n_tables = sizeof(all_tables) / sizeof(*all_tables);


static void fill_with_error(TableEntry *table, int n)
{
    for (int i = 0; i < n; i++)
    {
        table[i].terminal = G4_ERROR;
        table[i].length = 32; // this marks every read as incomplete, see g4-decode.c
    }
}

static void fill_all_with_error()
{
    for (int i = 0; i < n_tables; i++)
       fill_with_error(all_tables[i], 1 << all_table_log_sizes[i]); 
}

static int startswith(const char *str, int len, const char *prefix, int prefix_len)
{
    if (len < prefix_len)
        return false;
    return !strncmp(str, prefix, prefix_len);
}

static void fill_table(G4TableEntry *src, int src_length, 
                       TableEntry *dst, int dst_log_size)
{
    for (int i = 0; i < src_length; i++)
    {
        const char *code = src[i].code;
        int k = strlen(code);
        if (k > dst_log_size)
            continue;

        int v = code_value(code);
        int begin = v << (dst_log_size - k);
        int end = (v + 1) << (dst_log_size - k);
        for (int j = begin; j < end; j++)
        {
            dst[j].terminal = src[i].terminal;
            dst[j].length = k;
        }
    }
}

static void fill_secondary_table(G4TableEntry *src, int src_length,
                                 TableEntry *primary_dst, 
                                 int primary_log_size,
                                 TableEntry *secondary_dst,
                                 int secondary_log_size,
                                 const char *prefix,
                                 int jump)
{
    int prefix_len = strlen(prefix);
    for (int i = 0; i < src_length; i++)
    {
        const char *code = src[i].code;
        int k = strlen(code);
        int starts = startswith(code, k, prefix, prefix_len);
        if (!starts)
            continue;

        int v = code_value(code);
        int secondary_v = code_value(code + prefix_len);
        if (k > primary_log_size)
        {
            // insert a jump
            int primary_v = v >> (k - primary_log_size);
    
            primary_dst[primary_v].terminal = jump;
            primary_dst[primary_v].length = prefix_len; 
        }
#ifdef G4_USE_32BIT_HUFFMAN
        else
        {
            // we do have to jump
            // otherwise the decoder might ask for more bits but receive a jump instead
            // so yes, we jump even some small black codes
            int begin = v << (primary_log_size - k);
            int end = (v + 1) << (primary_log_size - k);
            for (int j = begin; j < end; j++)
            {
                primary_dst[j].terminal = jump;
                primary_dst[j].length = prefix_len;
            }
        }
#endif
        
        if (secondary_dst) // we don't have secondary_dst for EOFB
        {
            // we have to catch the jump even if we don't set it 
            // because of accidental jumps in the huffman32 decoder        
            int begin = secondary_v << (secondary_log_size - k + prefix_len);
            int end = (secondary_v + 1) << (secondary_log_size - k + prefix_len);
            for (int j = begin; j < end; j++)
            {
                secondary_dst[j].terminal = src[i].terminal;
                secondary_dst[j].length = k - prefix_len;
            }
        }
    }
}

static void print_table(TableEntry *values, int log_length)
{
    int length = 1 << log_length;
    enum { WRAP = 10 };
    for (int i = 0; i < length; i++)
    {
        if (i)
            printf(",");
        if (i % WRAP == 0)
            printf("\n    ");
        else
            printf(" ");
        printf("{%d, %d}", values[i].terminal, values[i].length);
    }
}
int main(int argc, const char *const *argv)
{
    if (argc != 2 || (strcmp(argv[1], "c") && strcmp(argv[1], "h")))
    {
        fprintf(stderr, "%s - generate Huffman tables for G4 decoding\n", argv[0]);
        fprintf(stderr, "Usage:\n");
        fprintf(stderr, "    %s c >g4-dtables.c\n", argv[0]);
        fprintf(stderr, "    %s h >g4-dtables.h\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    printf("// DO NOT EDIT - this file was generated by gen-dtables\n");
    
    fill_all_with_error();
    fill_table(main_codes, main_codes_length, main_table, G4_MAIN_TABLE_LOG_SIZE);
    fill_secondary_table(main_codes, main_codes_length,
                         main_table, G4_MAIN_TABLE_LOG_SIZE, NULL, 0, ESCAPE_EOFB, G4_JUMP_EOFB);

    fill_table(small_white_codes, small_white_codes_length, 
               white_table, G4_WHITE_TABLE_LOG_SIZE);
    fill_table(big_white_codes, big_white_codes_length, 
               white_table, G4_WHITE_TABLE_LOG_SIZE);
    fill_secondary_table(huge_codes, huge_codes_length,
               white_table, G4_WHITE_TABLE_LOG_SIZE, huge_run_table, G4_HUGE_RUN_TABLE_LOG_SIZE,
               ESCAPE_HUGE, G4_JUMP_HUGE_RUNS);

    fill_table(small_black_codes, small_black_codes_length, 
               black_table, G4_BLACK_TABLE_LOG_SIZE);
    fill_table(big_black_codes, big_black_codes_length, 
               black_table, G4_BLACK_TABLE_LOG_SIZE);
    // some small codes also start with ESCAPE_BLACK_LONG - we want to have them too
    fill_secondary_table(small_black_codes, small_black_codes_length, 
                         black_table, G4_BLACK_TABLE_LOG_SIZE, black_long_table, G4_BLACK_LONG_TABLE_LOG_SIZE,
                         ESCAPE_BLACK_LONG, G4_JUMP_BLACK_LONG);
    fill_secondary_table(big_black_codes, big_black_codes_length, 
                         black_table, G4_BLACK_TABLE_LOG_SIZE, black_long_table, G4_BLACK_LONG_TABLE_LOG_SIZE,
                         ESCAPE_BLACK_LONG, G4_JUMP_BLACK_LONG);
    fill_secondary_table(huge_codes, huge_codes_length,
               black_table, G4_BLACK_TABLE_LOG_SIZE, huge_run_table, G4_HUGE_RUN_TABLE_LOG_SIZE,
               ESCAPE_HUGE, G4_JUMP_HUGE_RUNS);

    if (!strcmp(argv[1], "c"))
    {
        printf("#include \"g4-dtables.h\"\n\n");
        printf("G4HuffmanTableEntry g4_main_table[] = {");
        print_table(main_table, G4_MAIN_TABLE_LOG_SIZE);
        printf("};\n\nG4HuffmanTableEntry g4_white_table[] = {");
        print_table(white_table, G4_WHITE_TABLE_LOG_SIZE);
        printf("};\n\nG4HuffmanTableEntry g4_black_table[] = {");
        print_table(black_table, G4_BLACK_TABLE_LOG_SIZE);
        printf("};\n\nG4HuffmanTableEntry g4_black_long_table[] = {");
        print_table(black_long_table, G4_BLACK_LONG_TABLE_LOG_SIZE);
        printf("};\n\nG4HuffmanTableEntry g4_huge_run_table[] = {");
        print_table(huge_run_table, G4_HUGE_RUN_TABLE_LOG_SIZE);
        puts("};");
    }
    else
    {
        // generation is not needed anymore
        puts("#ifndef G4_DTABLES_H");
        puts("#define G4_DTABLES_H");
        puts("");
        puts("typedef struct {");
        puts("    short terminal;");
        puts("    short length;");
        puts("} G4HuffmanTableEntry;");
        puts("");
        puts("extern G4HuffmanTableEntry g4_main_table[];");
        puts("extern G4HuffmanTableEntry g4_white_table[];");
        puts("extern G4HuffmanTableEntry g4_black_table[];");
        puts("extern G4HuffmanTableEntry g4_black_long_table[];");
        puts("extern G4HuffmanTableEntry g4_huge_run_table[];");
        puts("");
        puts("#endif");
    }

    return EXIT_SUCCESS;
}
