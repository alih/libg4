/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>

#include "g4-encode.h"
#include "g4-etables.h"
#include "g4-writer.h"

struct G4Writer
{
    G4Encoder *encoder;
    FILE *stream;
    int *line;
    int *prev_line;
    void (*finalizer)(void *);
    void *finalizer_ptr;
    int width;
    int height;
};

G4Writer *g4_writer_new(int width, int height)
{
    G4Writer *w = (G4Writer *) malloc(sizeof(G4Writer));
    memset(w, 0, sizeof(G4Writer));
    w->width = width;
    w->height = height;
    int m = width + 10; // XXX: dup from g4_reader_new
    w->line = (int *) malloc(m * sizeof(int));
    w->prev_line = (int *) malloc(m * sizeof(int));
    w->prev_line[0] = w->width;
    w->prev_line[1] = 0;
    return w;
}

int *g4_writer_rle(G4Writer *w)
{
    return w->line;
}

static void g4_writer_end_strip(G4Writer *w)
{
    g4_bitstream_write_EOFB(w->encoder);
    g4_bitstream_finish(w->encoder);
    g4_encoder_free(w->encoder);
    free(w->line);
    free(w->prev_line);
    w->encoder = NULL;
}

const char *g4_writer_error(G4Writer *w)
{
    (void) w;
    return NULL;
}

void g4_writer_free(G4Writer *w)
{
    if (w->encoder)
        g4_writer_end_strip(w);

    // finalization should be made after end_strip()!
    if (w->finalizer)
        w->finalizer(w->finalizer_ptr);

    free(w);
}

static void swap_lines(G4Writer *w)
{
    int *tmp = w->line;
    w->line = w->prev_line;
    w->prev_line = tmp;
}

void g4_writer_set_finalizer(G4Writer *w, void (*callback)(void *), void *callback_ptr)
{
    w->finalizer = callback;
    w->finalizer_ptr = callback_ptr;
}

void g4_writer_use_stdio(G4Writer *w, FILE *stream)
{
    w->stream = stream;
    w->encoder = g4_encoder_new(stream);
}

// ___________________________________________________________________________

void g4_writer_write_rle_line(G4Writer *w)
{
    g4_encode_line(w->encoder, w->line, w->prev_line, w->width);
    swap_lines(w);    
}

bool g4_write_line(G4Writer *w, const unsigned char *bitmap)
{
    g4_rle_extract(w->line, bitmap, w->width);
    g4_writer_write_rle_line(w);
    return true;
}

// ___________________________________________________________________________


static int rle_extract_int(int *runs, int32_t a, int k, int width)
{
    #define CLZ (lz = __builtin_clz(a), a = ~(a << lz), count -= lz, lz)
    assert(a > 0);
    int lz;
    int count = width;

    runs[k] += CLZ;

    while (count > 0)
        runs[++k] = CLZ;

    runs[k] += count; 

    return k;
    #undef CLZ
}
#if 0
// Extract runs from a single positive 32-bit integer.
int rle_extract_i32(int *runs, int32_t a, int k)
{
    #define CLZ (lz = __builtin_clz(a), a = ~(a << lz), count -= lz, lz)
    assert(a > 0);
    int lz;
    int count = sizeof(a) * 8;

    runs[k] += CLZ;

    while (count > 0)
        runs[++k] = CLZ;

    runs[k] += count; 

    return k;
    #undef CLZ
}
#endif
#define rle_extract_i32(RUNS, A, K) rle_extract_int(RUNS, A, K, 32)


static int rle_extract_one(int *runs,
                           int32_t a,
                           int k,
                           int bitwidth)
{
    a <<= 32 - bitwidth;
    if (k & 1)
    {
        // our current run is black
        if (a < -1)
            k = rle_extract_int(runs, ~a, k, bitwidth); // continue the current run
        else if (a == -1)
            runs[k] += bitwidth;
        else if (a > 0)
        {
            runs[++k] = 0;
            k = rle_extract_int(runs, a, k, bitwidth);
        }
        else // a == 0
            runs[++k] = bitwidth;
    }
    else // k & 1 == 0
    {
        // our current run is white
        if (a > 0)
            k = rle_extract_int(runs, a, k, bitwidth); // continue the current run
        else if (!a)
            runs[k] += bitwidth;
        else if (a < -1)
        {
            runs[++k] = 0;
            k = rle_extract_int(runs, ~a, k, bitwidth);
        }
        else // a == -1
            runs[++k] = bitwidth;
    }
    return k;
}

static int rle_extract32(int *runs,
                         const int32_t *bitmap,
                         int n,
                         int k)
{
    for (int i = 0; i < n; i++)
    {
        int32_t a = htonl(bitmap[i]);
        k = rle_extract_one(runs, a, k, 32);
        /*
        if (k & 1)
        {
            // our current run is black
            if (a < -1)
                k = rle_extract_i32(runs, ~a, k); // continue the current run
            else if (a == -1)
                runs[k] += sizeof(a) * 8;
            else if (a > 0)
            {
                runs[++k] = 0;
                k = rle_extract_i32(runs, a, k);
            }
            else // a == 0
                runs[++k] = sizeof(a) * 8;
        }
        else // k & 1 == 0
        {
            // our current run is white
            if (a > 0)
                k = rle_extract_i32(runs, a, k); // continue the current run
            else if (!a)
                runs[k] += sizeof(a) * 8;
            else if (a < -1)
            {
                runs[++k] = 0;
                k = rle_extract_i32(runs, ~a, k);
            }
            else // a == -1
                runs[++k] = sizeof(a) * 8;
        }*/
    } // for
    return k;
}


int g4_rle_extract(int *runs,
    const unsigned char *bitmap,
    int n_bits)
{
    if (!n_bits)
        return 0;
    int n = n_bits >> 3;
    int k = 0;
    runs[0] = 0;
    
    while (n && ((intptr_t) bitmap & 3))
    {
        k = rle_extract_one(runs, *bitmap, k, 8);
        bitmap++;
        n--;
    }
    if (n)
    {
        int n32 = n >> 2;
        // if we're here, then the loop above exited because bitmap & 3 is 0
        // which means bitmap is 4-aligned
        k = rle_extract32(runs, (const int32_t *) (const void *) bitmap, n32, k);
        bitmap += n & ~3;
        n &= 3;
        while (n)
        {
            k = rle_extract_one(runs, *bitmap, k, 8);
            bitmap++;
            n--;
        }
    }
    n_bits = n_bits & 7;
    if (n_bits)
        k = rle_extract_one(runs, *bitmap >> (8 - n_bits), k, n_bits);

    k++;
    if (k & 1)
        runs[k++] = 0;
    return k;
}
