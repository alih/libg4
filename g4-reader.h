/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef G4_READER_H
#define G4_READER_H

#include <stdbool.h>
#include <stdio.h>

typedef struct G4Reader G4Reader;

G4Reader *g4_reader_new(int width, int height, int strip_height);
void g4_reader_free(G4Reader *);
int g4_reader_n_strips(G4Reader *);

/**
 * Set a hook to be invoked by g4_reader_free().
 * This is used by G4Tiff to ensure consistency.
 */
void g4_reader_set_finalizer(G4Reader *, void (*callback)(void *), void *callback_ptr);

void g4_reader_use_stdio(G4Reader *, FILE *f);
void g4_reader_use_memory(G4Reader *, void *base);
void g4_reader_set_strip(G4Reader *, int strip_index, int offset, int length);

/**
 * Read the next RLE line.
 * \returns the number of runs or 0 on error.
 *
 * Access the runs by calling g4_reader_rle().
 */
int g4_reader_read_rle_line(G4Reader *);
int *g4_reader_rle(G4Reader *);
const char *g4_reader_error(G4Reader *);

void g4_rle_render(
    unsigned char *pixels,
    int n,
    const int *runs,
    int n_runs);

bool g4_read_line(G4Reader *, unsigned char *bitmap_row);

#endif
