#if 0

gcc -std=c99 -O3 -DNDEBUG -I. bench-decode.c -I. -Igenerated g4-*.c generated/g4-*.c -ltiff -o bench-decode
exit

#endif 

/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <tiffio.h>
#include "g4-tiff.h"

static void read_g4(const char *path)
{
    G4Tiff *g4 = g4_open(path, "r");
    int w = g4_width(g4);
    int h = g4_height(g4);
    G4Reader *r = g4_get_reader(g4);
    unsigned char bitmap[w];

    for (int i = 0; i < h; i++)
    {
        // g4_reader_read_rle_line(r);
        g4_read_line(r, bitmap);
    }

    g4_reader_free(r);
    g4_close(g4);
}

static void read_g4_rle(const char *path)
{
    G4Tiff *g4 = g4_open(path, "r");
    int h = g4_height(g4);
    G4Reader *r = g4_get_reader(g4);

    for (int i = 0; i < h; i++)
    {
        g4_reader_read_rle_line(r);
    }

    g4_reader_free(r);
    g4_close(g4);
}

static void read_tiff(const char *path)
{
    TIFF *tiff = TIFFOpen(path, "r");
    uint32 w, h;
    TIFFGetField(tiff, TIFFTAG_IMAGEWIDTH, &w);
    TIFFGetField(tiff, TIFFTAG_IMAGELENGTH, &h);
    tdata_t *bitmap = _TIFFmalloc(TIFFScanlineSize(tiff));

    for (uint32 i = 0; i < h; i++)
    {
        TIFFReadScanline(tiff, bitmap, i, 0);
    }

    _TIFFfree(bitmap);
    TIFFClose(tiff);
}

static void fill(uint8 *buf, uint32 *runs32, uint32 *erun32, uint32 width)
{
    (void) buf;
    (void) runs32;
    (void) erun32;
    (void) width;
}

static void read_tiff_rle(const char *path)
{
    TIFF *tiff = TIFFOpen(path, "r");
    uint32 w, h;
    TIFFGetField(tiff, TIFFTAG_IMAGEWIDTH, &w);
    TIFFGetField(tiff, TIFFTAG_IMAGELENGTH, &h);

    void *fill_func;
    if (TIFFGetField(tiff, TIFFTAG_FAXFILLFUNC, &fill_func))
    {
        TIFFSetField(tiff, TIFFTAG_FAXFILLFUNC, fill);
    }
    tdata_t *bitmap = _TIFFmalloc(TIFFScanlineSize(tiff));

    for (uint32 i = 0; i < h; i++)
    {
        TIFFReadScanline(tiff, bitmap, i, 0);
    }

    _TIFFfree(bitmap);
    TIFFClose(tiff);
}


int main(int argc, const char *const *argv)
{
    if (argc == 1)
    {
        fprintf(stderr, "%s - measure TIFF loading time\n", argv[0]);
        fprintf(stderr, "Usage:\n");
        fprintf(stderr, "    %s input1.tif input2.tif ...\n", argv[0]);
        exit(EXIT_FAILURE);
    }


    clock_t begin, end;

    begin = clock();
    for (int i = 1; i < argc; i++)
        read_g4(argv[i]);
    end = clock();
    printf("libg4 time: %lf\n", (double) (end - begin) / CLOCKS_PER_SEC);

    begin = clock();
    for (int i = 1; i < argc; i++)
        read_tiff(argv[i]);
    end = clock();
    printf("libtiff time: %lf\n", (double) (end - begin) / CLOCKS_PER_SEC);

    begin = clock();
    for (int i = 1; i < argc; i++)
        read_g4_rle(argv[i]);
    end = clock();
    printf("libg4 time (RLE only): %lf\n", (double) (end - begin) / CLOCKS_PER_SEC);

    begin = clock();
    for (int i = 1; i < argc; i++)
        read_tiff_rle(argv[i]);
    end = clock();
    printf("libtiff time (RLE only): %lf\n", (double) (end - begin) / CLOCKS_PER_SEC);

    return EXIT_SUCCESS;
}
