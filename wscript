APPNAME = 'libg4'
VERSION = '0.1'

top = '.'
out = 'build'

def options(opt):
    opt.load('compiler_c')

def configure(ctx):
    release_env = ctx.env
    ctx.load('compiler_c waf_unit_test')
    ctx.env.append_value('CFLAGS', ['--std=c99', '-pedantic-errors', '-D_ISOC99_SOURCE', 
                                    '-Wall', '-Wextra', '-Wshadow', '-Waggregate-return',
                                    '-Wmissing-prototypes', '-Wpointer-arith',
                                    '-Wredundant-decls', '-Wcast-qual', '-Wmissing-declarations'])
                                    #'-DG4_CODER_DEBUG_OUTPUT']) 
                                    #'-DNDEBUG', '-O3'])
    ctx.check(header='tiffio.h', lib='tiff', uselib_store='TIFF', 
              mandatory=False)

    ctx.setenv('debug', release_env)
    ctx.env.append_value('CFLAGS', ['-g'])

    ctx.setenv('coverage', release_env)
    ctx.env.append_value('CFLAGS', ['-g', '--coverage'])
    ctx.env.append_value('LINKFLAGS', '--coverage')

    release_env.append_value('CFLAGS', ['-O3', '-DNDEBUG']) # '-finline-limit=10000', 

# taken from the wafbook
from waflib.Build import BuildContext, CleanContext, \
                         InstallContext, UninstallContext
for x in 'debug coverage'.split():
    for y in (BuildContext, CleanContext, InstallContext, UninstallContext):
        name = y.__name__.replace('Context','').lower()
        class tmp(y): 
            if y == BuildContext:
                cmd = x
            else:
                cmd = name + '_' + x
            variant = x

def build(bld):
    bld.install_files('${PREFIX}/include/g4', [
        'g4.h',
        'g4-constants.h',
        'g4-dataio.h',
        'g4-decode.h',
        'g4-easyapi.h',
        'g4-encode.h',
        'g4-reader.h',
        'g4-tiff.h',
        'g4-writer.h',
    ])

    bld.objects(target='gen-common',
                source=['generator/gen-util.c',
                        'generator/codetable.c'],
                includes='#')
    
    bld.program(target='gen-etables', source='generator/gen-etables.c',
                use='gen-common',
                includes='#',
                install_path=None)

    bld.program(target='gen-dtables', source='generator/gen-dtables.c',
                use='gen-common',
                includes='#',
                install_path=None)

    bld.stlib(target='g4',
              source='generated/g4-dtables.c g4-decode.c g4-reader.c '
                     'generated/g4-etables.c g4-encode.c g4-writer.c '
                     'g4-dataio.c g4-tiff.c g4-easyapi.c',
              includes='# generated',
              install_path='${PREFIX}/lib')

    bld.program(target='g4topbm',
                source='main-g4topbm.c',
                use='g4')
    bld.program(target='pbmtog4',
                source='main-pbmtog4.c',
                use='g4')

    if bld.variant == 'debug' or bld.variant == 'coverage':
        from waflib.Tools import waf_unit_test
        bld.add_post_fun(waf_unit_test.summary)
        for source in bld.path.ant_glob('test/test-*.c'):
            bld.program(features = 'test',
                        target = source.name[:-2],
                        includes = '#',
                        use = 'g4 TIFF',
                        source = [source])


# vim: ft=python
