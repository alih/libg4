/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef G4_DATAIO_H
#define G4_DATAIO_H

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

typedef enum
{
    G4_TYPE_BYTE     = 1,
    G4_TYPE_CHAR     = 2,
    G4_TYPE_SHORT    = 3,
    G4_TYPE_LONG     = 4,
    G4_TYPE_RATIONAL = 5
} G4Type;

typedef struct
{
    uint16_t tag;
    uint16_t type;
    uint32_t count;
    uint32_t value;
} G4Entry;

typedef struct G4DataIO G4DataIO;

G4DataIO *g4_dataio_new(FILE *);
void g4_dataio_free(G4DataIO *);

/**
 * Read a TIFF directory.
 *
 * This doesn't read the arrays;
 * use g4_read_array() when count > 1.
 *
 * The tags in `entries' must be sorted but followed by 0.
 * This 0-tag entry will receive the offset to the next directory.
 */
bool g4_read_directory(G4DataIO *, G4Entry *entries);

/**
 * Read an array from a TIFF file.
 * This potentially seeks the stream.
 */
uint32_t *g4_read_array(G4DataIO *, G4Entry *entry);

void g4_align(G4DataIO *, int alignment);

/**
 * Write the TIFF directory.
 * The entries with zero count are skipped.
 *
 * If you want to patch the directory later,
 * record ftell() before the call,
 * then use g4_patch_entry() and g4_patch_offset().
 *
 * \returns the offset of the next directory pointer to be used with g4_patch()
 */
uint32_t g4_write_directory(G4DataIO *, G4Entry *entries);

uint32_t g4_read_tiff_header(G4DataIO *);
void g4_write_tiff_header(G4DataIO *);

void g4_patch(G4DataIO *, uint32_t offset, uint32_t value);

#endif
