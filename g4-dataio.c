/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "g4-dataio.h"

enum { ENTRY_SIZE = 12 };

struct G4DataIO
{
    FILE *stream;
    bool le;
};

static bool is_le()
{
    int test = 0x0201;
    char *p = (char *) &test;
    while (!*p)
        p++;
    return *p == 1;
}

G4DataIO *g4_dataio_new(FILE *stream)
{
    G4DataIO *io = (G4DataIO *) malloc(sizeof(G4DataIO));
    io->stream = stream;
    io->le = is_le(); // rewritten by g4_read_tiff_header()
    return io;
}

void g4_dataio_free(G4DataIO *io)
{
    free(io);
}

static const int size_by_type[] = 
    {0, 
     1,  // BYTE
     1,  // CHAR
     2,  // SHORT
     4,  // LONG
     8}; // RATIONAL

static uint8_t read8(G4DataIO *io)
{
    return fgetc(io->stream);
}

static uint16_t read16(G4DataIO *io)
{
    uint16_t a = fgetc(io->stream);
    uint16_t b = fgetc(io->stream);
    if (io->le)
        return (b << 8) | a;
    else
        return (a << 8) | b;
}

static uint32_t read32(G4DataIO *io)
{
    uint32_t a = fgetc(io->stream);
    uint32_t b = fgetc(io->stream);
    uint32_t c = fgetc(io->stream);
    uint32_t d = fgetc(io->stream);
    if (io->le)
        return (d << 24) | (c << 16) | (b << 8) | a;
    else
        return (a << 24) | (b << 16) | (c << 8) | d;
}

/* static void write8(G4DataIO *io, uint8_t value)
{
    fputc(value, io->stream);
} */

static void write16(G4DataIO *io, uint16_t value)
{
    fwrite(&value, 1, 2, io->stream);
}

static void write32(G4DataIO *io, uint32_t value)
{
    fwrite(&value, 1, 4, io->stream);
}

static int read_int(G4DataIO *io, G4Type type)
{
    switch (type)
    {
        case G4_TYPE_BYTE:
            return read8(io);
        case G4_TYPE_SHORT:
            return read16(io);
        case G4_TYPE_LONG:
            return read32(io);
        default:
            fprintf(stderr, "read_int() can't read type %d\n", type);
            exit(EXIT_FAILURE);
    }
}

uint32_t *g4_read_array(G4DataIO *io, G4Entry *entry)
{
    int type = entry->type;
    int count = entry->count;
    uint32_t value = entry->value;
    uint32_t size = size_by_type[type] * count;
    uint32_t *result = malloc(count * sizeof(uint32_t));
    if (size == 4 && count == 1)
    {
        result[0] = value;
    }
    else if (size <= 4)
    {
        // inline
        assert(size_by_type[type] < 4); // otherwise 1 << s doesn't work
        int s = size_by_type[type] * 8;
        uint32_t mask = (((uint32_t) 1) << s) - 1;
        for (int i = 0; i < count; i++)
            result[i] = (value >> (i * s)) & mask;
    }
    else
    {
        // by offset
        fseek(io->stream, value, SEEK_SET);

        for (int i = 0; i < count; i++)
            result[i] = read_int(io, type);
    }
    return result;
}

static int count_entries(G4Entry *entries)
{
    int result = 0;
    for (G4Entry *p = entries; p->tag; p++)
    {
        if (p->count)
            result++;
    }
    return result;
}

uint32_t g4_write_directory(G4DataIO *io, G4Entry *entries)
{
    write16(io, count_entries(entries));
    G4Entry *p;
    uint32_t offset = ftell(io->stream);
    for (p = entries; p->tag; p++)
    {
        if (!p->count)
            continue;

        // write entry
        write16(io, p->tag);
        write16(io, p->type);
        if (p->count > 1)
        {
            fprintf(stderr, "g4_write_directory() doesn't support arrays\n");
            exit(EXIT_FAILURE);
        }

        write32(io, p->count);
        uint32_t value_offset = offset + ENTRY_SIZE - 4;
        write32(io, p->value);
        p->value = value_offset;
        offset += ENTRY_SIZE;
    }
    write32(io, p->value); // next directory offset
    return offset;
}

// TIFF 6.0 Spec, Sec. 2, subsection "Image File Directory"
bool g4_read_directory(G4DataIO *io, G4Entry *entries)
{
    int n_entries = read16(io);

    long start = ftell(io->stream);
    for (int i = 0; i < n_entries; i++)
    {
        // read entry
        int tag   = read16(io);
        int type  = read16(io);
        uint32_t count = read32(io);
        uint32_t value = read32(io); 

        while (entries->tag && entries->tag < tag)
        {
            if (entries->value)
            {
                fprintf(stderr, "Required TIFF tag %d not found\n", entries->tag);
                return false;
            }
            entries->count = 0;
            entries++;
        }

        if (!entries->tag && i < n_entries - 1)
        {
            // we have read all we need, discard the rest
            if (fseek(io->stream, start + ENTRY_SIZE * n_entries, SEEK_SET))
                return false;
            break;
        }

        if (tag == entries->tag)
        {
            if (type != entries->type 
              && !((type == G4_TYPE_SHORT || type == G4_TYPE_BYTE) 
                   && entries->type == G4_TYPE_LONG)
              && !(type == G4_TYPE_BYTE
                   && entries->type == G4_TYPE_SHORT))
            {
                fprintf(stderr, 
                    "Value type mismatch for TIFF tag %d: expected %d, got %d\n",
                        tag, entries->type, type);
                errno = EINVAL;
                return false;
            }
            
            if (!count)
            {
                fprintf(stderr,
                    "Zero count for tag %d\n",
                        tag);
                errno = EINVAL;
                return false;
            }

            if (entries->count > 0 && count != entries->count)
            {
                fprintf(stderr,
                    "Value count mismatch for tag %d: expected %d, got %d\n",
                        tag, entries->count, count);
                errno = EINVAL;
                return false;
            }

            entries->type = type;
            entries->count = count;
            entries->value = value;
            
            entries++;
        } // if (tag == entries->tag)
    } // entry reading loop

    while (entries->tag)
    {
        if (entries->value)
        {
            fprintf(stderr, "Required TIFF tag %d not found\n", entries->tag);
            return false;
        }
        entries->count = 0;
        entries++;
    }

    entries->value = read32(io);
    return true;
}

/**
 * Read the header. Set `le'.
 * Returns the offset of the first directory or 0 on error.
 * See TIFF 6.0 Spec, Sec. 2, "Image File Header".
 */
uint32_t g4_read_tiff_header(G4DataIO *tiff)
{
    enum
    {
        MAGIC_BE = 0x4D4D002A,
        MAGIC_LE = 0x49492A00
    };

    tiff->le = false;
    uint32_t magic = read32(tiff);
    if (magic == MAGIC_LE)
        tiff->le = true;
    else if (magic != MAGIC_BE)
        return 0;

    return read32(tiff);
}


void g4_write_tiff_header(G4DataIO *io)
{
    write16(io, io->le ? 0x4949 : 0x4D4D);
    write16(io, 0x2A);
    write32(io, 8); // offset of the directory which follows immediately
}

void g4_align(G4DataIO *io, int alignment)
{
    long pos = ftell(io->stream);
    int q = pos % alignment;
    if (!q)
        return;
    char buf[alignment];
    memset(buf, 0, alignment);
    fwrite(buf, 1, alignment - q, io->stream);
}

void g4_patch(G4DataIO *io, uint32_t offset, uint32_t value)
{
    fseek(io->stream, offset, SEEK_SET);
    write32(io, value);
}
