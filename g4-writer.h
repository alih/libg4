/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef G4_WRITER_H
#define G4_WRITER_H

#include <stdbool.h>

/**
 * Extract the Run-Length Encoding representation from a bitmap row.
 *
 * \param[out] runs     The extracted RLE. Even runs are white, odd are black.
 *                      
 *                      Must have enough memory to contain (n + 2) runs.
 *                      
 *                      The first run may be 0 if the row starts with black;
 *                      the last run may be 0 if the row ends with white.
 *                      All the other runs will be at least 1.
 *
 * \param[in] bitmap    Bits packed into bytes, namely (n + 7) / 8 bytes, 
 *                      from the most significant bit to the least significant,
 *                      last byte potentially zero padded.
 *
 * \param n             The number of bits (not bytes!).
 *
 * \returns             The number of runs. It will always be an even number.
 */
int g4_rle_extract(int *runs,
         const unsigned char *bitmap,
         int n);

// ____________________________________________

typedef struct G4Writer G4Writer;

void g4_writer_free(G4Writer *);

const char *g4_writer_error(G4Writer *);

void g4_writer_write_rle_line(G4Writer *);
int *g4_writer_rle(G4Writer *);

bool g4_write_line(G4Writer *, const unsigned char *bitmap_row);

// ____________________________________________

// The following functions are called by G4Tiff so normally you don't need to.

G4Writer *g4_writer_new(int width, int height);
void g4_writer_use_stdio(G4Writer *, FILE *f);
/**
 * Set a hook to be invoked by g4_reader_free().
 * This is used by G4Tiff to ensure consistency.
 */
void g4_writer_set_finalizer(G4Writer *, void (*callback)(void *), void *callback_ptr);


#endif
