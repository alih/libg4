/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef G4_CONSTANTS_H
#define G4_CONSTANTS_H

// Some constants defined in the G4 specification.
// This should correspond to name_table in gen-etable.c.
enum
{
    G4_PASS = 10,
    G4_HORIZONTAL = 11,
    G4_EXTENSION = 12,
    G4_EOFB = 13
};

// The range of the opcodes for the vertical mode.
enum
{
    G4_MIN_V = -3,
    G4_MAX_V = 3
};

// Microterminals (codes inserted into Huffman table for optimization)
enum
{
    G4_JUMP_BLACK_LONG = -4,
    G4_JUMP_HUGE_RUNS = -5,
    G4_ERROR = -6,
    G4_JUMP_EOFB = -7
};

// Table parameters
enum
{
    G4_MAX_SMALL_VALUE = 63,
    G4_BIG_TABLE_STEP = G4_MAX_SMALL_VALUE + 1,
    G4_MAX_TABLE_RUN = 2560
};

// Huffman table sizes
enum
{
    G4_MAIN_TABLE_LOG_SIZE = 7,
    G4_WHITE_TABLE_LOG_SIZE = 9,
    G4_BLACK_TABLE_LOG_SIZE = 12,
    G4_BLACK_LONG_TABLE_LOG_SIZE = 6,
    G4_HUGE_RUN_TABLE_LOG_SIZE = 4
};

#endif
