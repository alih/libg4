/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "g4-decode.h"
#include "g4-reader.h"

struct G4Reader
{
    int *line;
    int *prev_line;

    G4Decoder *decoder;

    FILE *stream;
    void *data; // doubles as a buffer

    int *strip_offsets;
    int *strip_lengths;

    void (*finalizer)(void *);
    void *finalizer_ptr;

    const char *error;

    int width;
    int height;
    int n_strips;
    int strip_height;

    int current_strip;
    int current_line;
    int lines_left_in_strip;

    int strip_buf_allocated;
};

G4Reader *g4_reader_new(int width, int height, int strip_height)
{
    G4Reader *r = (G4Reader *) malloc(sizeof(G4Reader));
    memset(r, 0, sizeof(G4Reader));

    int m = width + 10; // why 10?
    r->line = (int *) malloc(m * sizeof(int));
    r->prev_line = (int *) malloc(m * sizeof(int));

    r->width = width;
    r->height = height;
    r->strip_height = strip_height;
    r->n_strips = (height + strip_height - 1) / strip_height;
    r->strip_offsets = (int *) malloc(r->n_strips * sizeof(int));
    r->strip_lengths = (int *) malloc(r->n_strips * sizeof(int));

    r->current_strip = -1;
    r->current_line = -1;

    return r;
}

int g4_reader_n_strips(G4Reader *r)
{
    return r->n_strips;
}

void g4_reader_set_finalizer(G4Reader *r, void (*callback)(void *), void *callback_ptr)
{
    r->finalizer = callback;
    r->finalizer_ptr = callback_ptr;
}

void g4_reader_use_stdio(G4Reader *r, FILE *f)
{
    r->stream = f;
}

void g4_reader_use_memory(G4Reader *r, void *ptr)
{
    r->data = ptr;
}

void g4_reader_set_strip(G4Reader *r, int strip_index, int offset, int length)
{
    r->strip_offsets[strip_index] = offset;
    r->strip_lengths[strip_index] = length;
}

const char *g4_reader_error(G4Reader *r)
{
    return r->decoder ? g4_decoder_error(r->decoder) 
                      : NULL;
}

void g4_reader_free(G4Reader *r)
{
    if (r->finalizer)
        r->finalizer(r->finalizer_ptr);
    if (r->decoder)
        g4_decoder_free(r->decoder);
    free(r->line);
    free(r->prev_line);
    if (r->stream && r->data)
        free(r->data);
    if (r->strip_offsets)
        free(r->strip_offsets);
    if (r->strip_lengths)
        free(r->strip_lengths);
    free(r);
}

static int get_strip_height(G4Reader *r, int index)
{
    int h = r->strip_height;
    if (index == r->n_strips - 1)
        h = r->height - h * (r->n_strips - 1);
    if (h > r->strip_height)
        h = r->strip_height;
    return h;
}

static void swap_lines(G4Reader *r)
{
    int *tmp = r->line;
    r->line = r->prev_line;
    r->prev_line = tmp;
}

static void ensure_buffer(G4Reader *r, int length)
{
    if (r->strip_buf_allocated >= length)
        return;
    if (r->data)
        r->data = realloc(r->data, length);
    else
        r->data = malloc(length);
    r->strip_buf_allocated = length;
}

static void next_strip(G4Reader *r)
{
    if (!r->strip_offsets || !r->strip_lengths)
    {
        fprintf(stderr, "Strip offsets/lengths not set in the G4Reader\n");
        exit(EXIT_FAILURE);
    }

    if (!r->strip_lengths[r->current_strip])
    {
        fprintf(stderr, "G4Reader: current strip is empty\n");
        exit(EXIT_FAILURE);
    }

    int offset = r->strip_offsets[r->current_strip];
    int length = r->strip_lengths[r->current_strip];

    if (!r->stream)
        r->decoder = g4_decoder_new((unsigned char *) r->data + offset, length);
    else
    {
        ensure_buffer(r, length);
        fseek(r->stream, offset, SEEK_SET);
        int read = fread(r->data, 1, length, r->stream);
        if (read != length)
        {
            fprintf(stderr, "Error in G4Reader while reading a strip from file\n");
            exit(EXIT_FAILURE);
        }
        r->decoder = g4_decoder_new(r->data, length);
    }
    g4_decoder_set_strip_id(r->decoder, r->current_strip);
    r->lines_left_in_strip = get_strip_height(r, r->current_strip);
    r->line[0] = r->width; // a completely white line for the next g4_decode_line
    r->line[1] = 0;
}

int g4_reader_read_rle_line(G4Reader *r)
{
    // printf("Reader: lines_left_in_strip %d\n", r->lines_left_in_strip);
    if (!r->lines_left_in_strip)
    {
        if (r->current_strip >= r->n_strips - 1)
        {
            // we're out of data
            return 0;
        }
        
        r->current_strip++;
        next_strip(r);
    }
    
    swap_lines(r);
    int result = g4_decode_line(r->decoder, r->line, r->prev_line, r->width);
    
    if (result)
    {
        if (!--r->lines_left_in_strip)
        {
            g4_decoder_free(r->decoder);
            r->decoder = NULL;
        }
    }

    return result;
}

int *g4_reader_rle(G4Reader *r)
{
    return r->line;
}

void g4_rle_render(
    unsigned char *pixels,
    int n,
    const int *runs,
    int n_runs)
{
    assert(n_runs % 2 == 0);
    memset(pixels, 0, (n + 7) >> 3);
    int x = 0;
    for (int i = 0; i < n_runs; i += 2)
    {
        x += runs[i];
        int si = x >> 3;
        int sk = x & 7;
        int black_run = runs[i + 1];
        if (!black_run)
            continue;
        x += black_run;
        assert(x <= n);
        int ei = (x - 1) >> 3;
        int ek = (x - 1) & 7;
        if (si == ei)
            pixels[si] |= ((1 << black_run) - 1) << (7 - ek);
        else
        {
            pixels[si] |= 0xFF >> sk;
            for (int j = si + 1; j < ei; j++)
                pixels[j] = 255;
            pixels[ei] = 0xFF80 >> ek;
        }
    }
}

bool g4_read_line(G4Reader *r, unsigned char *row)
{
    int n_runs = g4_reader_read_rle_line(r);
    if (!n_runs)
        return false;
    g4_rle_render(row, r->width, r->line, n_runs);
    return true;
}
