/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "g4-dataio.h"
#include "g4-tiff.h"

enum
{
    COMPRESSION_G4 = 4
};

typedef struct
{
    // must be ordered by the tag
#define FIRST_INTERESTING_ENTRY width // used for pointer conversions (most likely unnecessary)
    G4Entry width;
    G4Entry height;
    G4Entry compression;
    G4Entry inverted;
    G4Entry strip_offsets;
    G4Entry strip_height;
    G4Entry strip_lengths;
    G4Entry x_resolution;
    G4Entry y_resolution;
    G4Entry resolution_unit;
    G4Entry next_offset;
} InterestingEntries;


struct G4Tiff
{
    FILE *stream;
    G4DataIO *dataio;
    uint32_t *strip_offsets;
    uint32_t *strip_lengths;
    const char *error;

    InterestingEntries entries; // this dups almost everything - should we get rid of other fields?

    uint32_t directory_nextptr_offset;
    uint32_t next_offset;
   /* double x_dpi;
    double y_dpi; */
    int page_no;
    int width;
    int height;
    int strip_height;
    int n_strips;
    bool writing;
    bool inverted;
    bool page_is_open; // that means that a G4Reader or G4Writer still exists
    bool pending_call_next; // the API awaits a call to g4_next() or g4_close()
};

// __________________   accessors   _______________________


int g4_width(G4Tiff *g4)
{
    return g4->width;
}

int g4_height(G4Tiff *g4)
{
    return g4->height;
}

void g4_set_size(G4Tiff *g4, int width, int height)
{
    if (!g4->writing)
    {
        fprintf(stderr, "Setting size while reading G4\n");
        exit(EXIT_FAILURE);
    }
    g4->width = width;
    g4->height = height;
}

const char *g4_error(G4Tiff *g4)
{
    return g4->error;
}

/*int g4_page_no(G4Tiff *g4)
{
    return g4->page_no;
}*/

int g4_strip_height(G4Tiff *g4)
{
    return g4->strip_height;
}

int g4_strip_count(G4Tiff *g4)
{
    return g4->n_strips;
}

int g4_strip_offset(G4Tiff *g4, int strip_index)
{
    if (strip_index < 0 || strip_index >= g4->n_strips)
        return 0;
    return g4->strip_offsets[strip_index];
}
int g4_strip_length(G4Tiff *g4, int strip_index)
{
    if (strip_index < 0 || strip_index >= g4->n_strips)
        return 0;
    return g4->strip_lengths[strip_index];
}

// ________________________________________________________

#if 0 // not supporting next() for now
static bool g4_next_read(G4Tiff *g4)
{
    XXXXXXXX
}

static void g4_next_write(G4Tiff *g4)
{
    // The G4Writer has finished writing the stream
    patch_tiff_directory(g4);
}

bool g4_next(G4Tiff *g4)
{
    XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    if (g4->writing)
        g4_next_write(g4);
    else
        return g4_next_read(g4);
    return true;
}

bool g4_has_next(G4Tiff *g4)
{
    return g4->next_offset != 0;
}

bool g4_go_to_page(G4Tiff *g4, int page_no)
{
    if (g4->page_no > page_no)
        return false;
    while (g4->page_no < page_no)
    {
        if (!g4_has_next(g4))
            return false;
        g4_next(g4);
    }
    return true;
}
#endif

void g4_dump(G4Tiff *tiff)
{
    printf("Page #%d\n", tiff->page_no);
    printf("Size %d x %d\n", tiff->width, tiff->height);
    printf("Next page offset: %u\n", tiff->next_offset);
    printf("Number of strips: %d\n", tiff->n_strips);
    printf("Rows per strip: %d\n", tiff->strip_height);
/*    printf("X dpi: %g\n", tiff->x_dpi);
    printf("Y dpi: %g\n", tiff->y_dpi);*/
    if (tiff->n_strips)
    {
        printf("Strips' offsets and lengths:\n");
        for (int i = 0; i < tiff->n_strips; i++)
        {
            printf("    %8u %8u\n", tiff->strip_offsets[i], 
                                    tiff->strip_lengths[i]);
        }
    }
}


// _________________________   read TIFF tag values   _________________________

/*
static double read_rational(G4Tiff *tiff)
{
    uint32_t offset = read32(tiff);
    long pos = ftell(tiff->stream);
    fseek(tiff->stream, offset, SEEK_SET);

    uint32_t num = read32(tiff);
    uint32_t den = read32(tiff);

    fseek(tiff->stream, pos, SEEK_SET);
    return ((double) num) / den;
}*/

/*static int read_int_padded(G4Tiff *tiff, Type type)
{
    int result = read_int(tiff, type);
    for (int i = size_by_type[type]; i < 4; i++)
        fgetc(tiff->stream);
}*/

// here value means that the tag is required
InterestingEntries entries_template = {
    .width           = {.tag = 256, /* ImageWidth */ 
                        .type = G4_TYPE_LONG, .count = 1, .value = 1},
    .height          = {.tag = 257, /* ImageLength */
                        .type = G4_TYPE_LONG, .count = 1, .value = 1},
    .compression     = {.tag = 259, /* Compression */
                        .type = G4_TYPE_SHORT, .count = 1, .value = 1},
    .inverted        = {.tag = 262, /* PhotometricInterpretation */
                        .type = G4_TYPE_SHORT, .count = 1, .value = 0},
    .strip_offsets   = {.tag = 273, /* StripOffsets */
                        .type = G4_TYPE_LONG, .count = 0, .value = 1},
    .strip_height    = {.tag = 278, /* RowsPerStrip */
                        .type = G4_TYPE_LONG, .count = 0, .value = 0},
    .strip_lengths   = {.tag = 279, /* StripByteCounts */
                        .type = G4_TYPE_LONG, .count = 0, .value = 1},
    .x_resolution    = {.tag = 282, /* XResolution */
                        .type = G4_TYPE_RATIONAL, .count = 1, .value = 0},
    .y_resolution    = {.tag = 283, /* YResolution */
                        .type = G4_TYPE_RATIONAL, .count = 1, .value = 0},
    .resolution_unit = {.tag = 296, /* ResolutionUnit */
                        .type = G4_TYPE_SHORT, .count = 1, .value = 0},
    .next_offset     = {.tag = 0,
                        .type = G4_TYPE_LONG, .count = 1, .value = 0}
};

static void write_tiff_directory(G4Tiff *tiff)
{
    g4_align(tiff->dataio, 8);

    tiff->entries = entries_template;
    tiff->entries.width.value = tiff->width;
    tiff->entries.height.value = tiff->height;
    tiff->entries.compression.value = COMPRESSION_G4;
    tiff->entries.inverted.value = 0;
    tiff->entries.strip_offsets.count = 1; // to be patched later
    tiff->entries.strip_lengths.count = 1; // --//--
    tiff->entries.x_resolution.count = 0;
    tiff->entries.y_resolution.count = 0;
    tiff->entries.resolution_unit.count = 0;

    tiff->directory_nextptr_offset = g4_write_directory(tiff->dataio, &tiff->entries.FIRST_INTERESTING_ENTRY);
}

static void patch_tiff_directory(G4Tiff *tiff)
{
    g4_patch(tiff->dataio,
             tiff->entries.strip_offsets.value,
             tiff->strip_offsets[0]);
    g4_patch(tiff->dataio,
             tiff->entries.strip_lengths.value,
             tiff->strip_lengths[0]);
    if (tiff->next_offset)
    {
        g4_patch(tiff->dataio,
                 tiff->directory_nextptr_offset,
                 tiff->next_offset);
    }
}

// See TIFF 6.0 Spec, Sec. 3 "Bilevel Images"
// Not to be confused with g4_read_directory(), which is tag-agnostic.
static bool read_tiff_directory(G4Tiff *tiff, uint32_t offset)
{
    if (fseek(tiff->stream, offset, SEEK_SET))
        return false;

    tiff->entries = entries_template;

    G4Entry *entries = &tiff->entries.FIRST_INTERESTING_ENTRY;
    if (!g4_read_directory(tiff->dataio, entries))
        return false;

    tiff->width = tiff->entries.width.value;
    tiff->height = tiff->entries.height.value;
    if (tiff->entries.compression.value != COMPRESSION_G4)
    {
        fprintf(stderr, "Only G4 compression supported\n");
        return false;
    }
    tiff->inverted = tiff->entries.inverted.count && tiff->entries.inverted.value;
    tiff->strip_height = tiff->entries.strip_height.count 
                                ? (int) tiff->entries.strip_height.value 
                                : tiff->height;
    if (tiff->entries.strip_offsets.count != tiff->entries.strip_lengths.count)
    {
        fprintf(stderr, "Strip offsets and lengths count mismatch\n");
        return false;
    }

    tiff->n_strips = tiff->entries.strip_offsets.count;
    tiff->strip_offsets = g4_read_array(tiff->dataio, &tiff->entries.strip_offsets);
    tiff->strip_lengths = g4_read_array(tiff->dataio, &tiff->entries.strip_lengths);

    // XXX resolution
    
    return true;
}

// ___________________________________________________________________________

G4Tiff *g4_open(const char *path, const char *mode)
{
    if (strcmp(mode, "r") && strcmp(mode, "w"))
    {
        fprintf(stderr, "g4_open(): only \"r\" and \"w\" modes supported\n");
        errno = EINVAL;
        return NULL;
    }
    G4Tiff *result = (G4Tiff *) malloc(sizeof(G4Tiff));
    if (!result)
    {
        errno = ENOMEM;
        return NULL;
    }
    memset(result, 0, sizeof(G4Tiff));
    bool writing = result->writing = *mode == 'w';

    FILE *stream = fopen(path, writing ? "wb" : "rb");
    if (!stream)
    {
        free(result);
        return NULL; // errno is set by fopen()
    }

    result->stream = stream;
    result->page_no = 1;
    result->dataio = g4_dataio_new(stream);

    if (writing)
    {
        result->strip_offsets = (uint32_t *) malloc(sizeof(uint32_t));
        result->strip_lengths = (uint32_t *) malloc(sizeof(uint32_t));
        *result->strip_offsets = 0;
        *result->strip_lengths = 0;
        result->n_strips = 1;
    }
    else
    {
        result->strip_offsets = NULL;
        result->strip_lengths = NULL;
    }
    
    if (!writing)
    {
        uint32_t offset = g4_read_tiff_header(result->dataio);
        if (!offset)
        {
            errno = EILSEQ;
            return NULL;
        }

        bool ok = read_tiff_directory(result, offset);
        if (!ok)
        {
            int errno_save = errno;
            fclose(stream);
            free(result);
            errno = errno_save;
            return NULL;
        }
    }
    else
    {
        result->next_offset = 0;
        result->width = -1;
        result->height = -1;
    }

    return result;
}

void g4_close(G4Tiff *g4)
{
    if (g4->page_is_open)
        fprintf(stderr, "Error: closing G4Tiff before freeing G4Reader/G4Writer\n");
    else if (g4->writing)
        patch_tiff_directory(g4);

    g4_dataio_free(g4->dataio);
    fclose(g4->stream);
    if (g4->strip_offsets)
        free(g4->strip_offsets);
    if (g4->strip_lengths)
        free(g4->strip_lengths);
    free(g4);
}

static void on_reader_free(void *ptr)
{
    G4Tiff *g4 = (G4Tiff *) ptr;
    g4->page_is_open = false;
}

static void on_writer_free(void *ptr)
{
    on_reader_free(ptr);
    G4Tiff *g4 = (G4Tiff *) ptr;
    g4->strip_lengths[0] = ftell(g4->stream) - g4->strip_offsets[0];
}

G4Reader *g4_get_reader(G4Tiff *g4)
{
    if (g4->writing)
    {
        g4->error = "Reading from a write-only TIFF";
        return NULL;
    }

    if (g4->page_is_open)
    {
        g4->error = "Opening a G4Reader while the previous one still exists";
        return NULL;
    }

    // Not checking pending_call_next: 
    // it's perfectly legal (albeit stupid) 
    // to read the same page twice.
    
    G4Reader *r = g4_reader_new(g4->width, g4->height, g4->strip_height);
    int r_n_strips = g4_reader_n_strips(r);
    if (g4->n_strips != r_n_strips)
    {
        g4_reader_free(r);
        g4->error = "Strip count is inconsistent with strip height";
        return NULL;
    }
    for (int i = 0; i < r_n_strips; i++)
    {
        g4_reader_set_strip(r, i, g4->strip_offsets[i],
                                  g4->strip_lengths[i]);
    }

    g4_reader_set_finalizer(r, on_reader_free, g4);
    g4_reader_use_stdio(r, g4->stream);

    return r;
}

G4Writer *g4_get_writer(G4Tiff *g4)
{
    if (!g4->writing)
    {
        g4->error = "Writing to a read-only TIFF";
        return NULL;
    }
    
    // the file is still empty
    g4_write_tiff_header(g4->dataio); // XXX

    write_tiff_directory(g4);

    G4Writer *w = g4_writer_new(g4->width, g4->height);
    g4->strip_offsets[0] = ftell(g4->stream);
    g4_writer_use_stdio(w, g4->stream);
    g4_writer_set_finalizer(w, on_writer_free, g4);
    return w;
}
