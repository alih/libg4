/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef G4_ENCODE_H
#define G4_ENCODE_H

#include <stdio.h>

typedef struct G4Encoder G4Encoder;

G4Encoder *g4_encoder_new(FILE *stream);
void g4_encoder_free(G4Encoder *);

void g4_encode_line(G4Encoder *e, const int *current_line, const int *upper_line, int width);

/**
 * Write bits into the bitstream.
 * This is exported because G4Writer needs to write EOFB and finish.
 */
void g4_bitstream_write(G4Encoder *e, int val, int len);
void g4_bitstream_finish(G4Encoder *e);

#endif
