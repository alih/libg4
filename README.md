Libg4 is a clean room reimplementation of CCITT Group 4 aka G4 compression.
Libg4 decodes and encodes single-page black-and-white TIFF files.

On encoding, libg4 is a lot faster (2.3x on my machine) than libtiff.
On decoding, libg4 is significantly faster (~25% on my machine) than libtiff.

If you want to save an RLE-encoded image, then libg4 (unlike libtiff) offers
an opportunity to do so without unpacking and repacking it.

Building
--------
The short of it is:

    ./waf configure
    ./waf

A longer version of it can be found in the INSTALL file.


Using
-----

There are 2 sample executables built:

    build/pbmtog4
    build/g4topbm

which do what you'd think they do. 

The library is built at build/libg4.a 
and I haven't bothered with a shared library yet.
Add the libg4 directory to the include directories list and include libg4 thus:

    #include <g4.h>

Link with libg4.a.

Libg4 provides a simple API to get things done quickly. Here's an example of saving:

```c
    bool ok = g4_save("foo.tif", image, width, height, stride);
    if (!ok)
        exit(EXIT_FAILURE);
```

And here's an example of loading:

```c
    int width;
    int height;
    int stride;
    unsigned char *image = g4_load("foo.tif", 
                                   /* desired alignment: */ 4,
                                   &width, &height, &stride);
    if (!image)
        exit(EXIT_FAILURE);

    ...

    g4_free(image);
```

Testing
-------

It is only possible to build tests when libtiff is installed.

    ./waf debug

should build the tests and run them. 

Benchmarking
------------

There are 2 benchmark programs built at
    build/bench-decode
    build/bench-encode

Each expects a bunch of TIFF pages.
bench-decode will simply load all of them once.
bench-encode will write each of them 100 times to /dev/null, that will take a lot longer.

Porting
-------

libg4 uses:
- standard C99
- converting (char *) to (int *) if alignment allows
- htonl()/ntohl() from POSIX standard
- __builtin_clz() understood by GCC and Clang
