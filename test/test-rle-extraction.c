/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#undef NDEBUG // for testing

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>

#include <g4.h>

// ___________________________________________________________________________


static int rle_extract_dumb(int *runs,
                            const unsigned char *bitmap,
                            int n)
{
    int coef = 0x80;
    int a = *bitmap;
    int run = 0;
    int color = 0;
    int *p = runs;

    while (n--)
    {
        if ((a & coef ? 1 : 0) == color)
        {
            run++;
        }
        else
        {
            *p++ = run;
            color = !color;
            run = 1;
        }

        coef >>= 1;
        if (!coef)
        {
            coef = 0x80;
            if (!n) break; // stay within the memory in the next statement
            a = *++bitmap;
        }
    }

    if (run)
        *p++ = run;

    if ((p - runs) & 1)
        *p++ = 0;

    return p - runs;
}

uint8_t data[1000];
int runs[8010];
int runs2[8010];
int main()
{
    for (int iter = 0; iter < 10000; iter++)
    {
        int n = rand() % 256;
        int n_bytes = (n + 7) >> 3;
        for (int i = 0; i < n_bytes; i++)
        {
            data[i] = rand() % 256;
        }
        int k = g4_rle_extract(runs, data, n);
        int k2 = rle_extract_dumb(runs2, data, n);
        assert(k == k2);
        for (int i = 0; i < k; i++)
        {
            // printf("runs[%d]=%d runs2[%d]=%d\n", i, runs[i], i, runs2[i]);
            assert(runs[i] == runs2[i]);
        }
    }

    return 0;
}
