#undef NDEBUG // for testing

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <tiffio.h>
#include "g4.h"

static void save_with_libtiff(const char *path, unsigned char *pixels, int w, int h, int stride)
{
    TIFF *tif = TIFFOpen(path, "w");
    
    TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, w);
    TIFFSetField(tif, TIFFTAG_IMAGELENGTH, h);
    TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 1);
    TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);
    TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_CCITTFAX4);
    TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISWHITE); // XXX test this crap maybe?

    for (int i = 0; i < h; i++)
        TIFFWriteScanline(tif, pixels + i * stride, i, 0);

    TIFFClose(tif);
}

static unsigned char *load_with_libtiff(const char *path, int alignment, 
                                 int *out_width, 
                                 int *out_height, 
                                 int *out_stride)
{
    if (alignment < 0)
        return NULL;

    TIFF *tif = TIFFOpen(path, "r");

    uint32 samples_per_pixel = 1;
    uint32 bits_per_sample = 1; 

    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bits_per_sample);
    TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samples_per_pixel);

    assert(samples_per_pixel == 1);
    assert(bits_per_sample == 1);

    uint32 width;
    uint32 height;
    TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &height);

    int photometric = PHOTOMETRIC_MINISWHITE;
    TIFFGetField(tif, TIFFTAG_PHOTOMETRIC, &photometric);
    // XXX do something with this photometric crap

    if (!alignment)
        alignment = 1;
    int stride = (width + 7) >> 3;
    stride += alignment - 1;
    stride -= stride % alignment;
    *out_width = width;
    *out_height = height;
    *out_stride = stride;

    unsigned char *image = (unsigned char *) _TIFFmalloc(stride * height);

    for (unsigned i = 0; i < height; i++)
        TIFFReadScanline(tif, image + i * stride, i, 0);

    TIFFClose(tif);
    return image;
}

static void free_loaded_with_libtiff(unsigned char *ptr)
{
    _TIFFfree(ptr);
}

static void assert_rows_equal(unsigned char *row1, unsigned char *row2,
                      int width)
{
    int width_in_bytes = (width + 7) >> 3;
    int last_byte_bits = width & 7;
    if (!last_byte_bits)
    {
        assert(!memcmp(row1, row2, width_in_bytes));
        return;
    }

    if (width_in_bytes >= 1)
        assert(!memcmp(row1, row2, width_in_bytes - 1));
    int a = row1[width_in_bytes - 1] >> (8 - last_byte_bits);
    int b = row2[width_in_bytes - 1] >> (8 - last_byte_bits);
    assert(a == b);
}

static void assert_images_equal(unsigned char *pixels1,
                         unsigned char *pixels2, 
                         int w, int h, 
                         int stride1, int stride2)
{
    for (int y = 0; y < h; y++)
        assert_rows_equal(pixels1 + y * stride1, pixels2 + y * stride2, w);
}

static void roundtrip_libtiff_to_g4(const char *path, unsigned char *pixels, 
                             int w, int h, int stride)
{
    save_with_libtiff(path, pixels, w, h, stride);
    
    int t_w;
    int t_h;
    int t_stride;   
    unsigned char *t_pixels = g4_load(path, 4, &t_w, &t_h, &t_stride);

    assert(t_w == w);
    assert(t_h == h);
    assert_images_equal(pixels, t_pixels, w, h, stride, t_stride);

    g4_free(t_pixels);
}

static void roundtrip_g4_to_libtiff(const char *path, unsigned char *pixels, 
                             int w, int h, int stride)
{
    g4_save(path, pixels, w, h, stride);
    
    int t_w;
    int t_h;
    int t_stride;   
    unsigned char *t_pixels = load_with_libtiff(path, 4, &t_w, &t_h, &t_stride);

    assert(t_w == w);
    assert(t_h == h);
    assert_images_equal(pixels, t_pixels, w, h, stride, t_stride);

    free_loaded_with_libtiff(t_pixels);
}

static void roundtrip_both_ways(const char *path, unsigned char *pixels, 
                         int w, int h, int stride)
{
    roundtrip_libtiff_to_g4(path, pixels, w, h, stride);
    roundtrip_g4_to_libtiff(path, pixels, w, h, stride);
}

static void test_random_image(const char *test_path, int w, int h)
{
    int stride = (w + 7) >> 3;
    unsigned char buf[stride * h];
    for (int i = 0; i < stride * h; i++)
        buf[i] = (unsigned char) rand();

    roundtrip_both_ways(test_path, buf, w, h, stride);
}

static void unpack(unsigned char *result, int w, int h, int stride, int packed)
{
    for (int y = 0; y < h; y++)
    {
        memset(result + y * stride, 0, stride);
        for (int x = 0; x < w; x++)
        {
            if (packed & (1 << x))
                result[x >> 3] |= 1 << (x & 7);
        }
    }
}

static void test_all_images(const char *test_path, int w, int h)
{
    int stride = (w + 7) >> 3;
    int n = 1 << (w * h);
    unsigned char buf[stride * h];

    for (int i = 0; i < n; i++)
    {
        unpack(buf, w, h, stride, i);
        roundtrip_both_ways(test_path, buf, w, h, stride);
    }
}

static void warn(const char *name, const char *fmt, va_list list)
{
    fprintf(stderr, "Warning: %s:", name);
    vfprintf(stderr, fmt, list);
    fputc('\n', stderr);
    exit(EXIT_FAILURE);
}

int main(int argc, const char *const *argv)
{
    TIFFSetWarningHandler(warn);

    const char *test_path = "/dev/shm/g4-test.tif";

    if (argc == 3 && !strcmp(argv[1], "-t"))
    {
        test_path = argv[2];
        argc = 1;
    }

    if (argc == 1)
    {
        for (int w = 1; w < 8; w++)
            for (int h = 1; h < 8 - w; h++)
                test_all_images(test_path, w, h);

        for (int iter = 0; iter < 1000; iter++)
        {
            int w = rand() % 20 + 1;
            int h = rand() % 20 + 1;
            test_random_image(test_path, w, h);
        }
    }
    else
    {
        int w;
        int h;
        int stride;   
        unsigned char *pixels = g4_load(argv[1], 1, &w, &h, &stride);

        roundtrip_both_ways(test_path, pixels, w, h, stride);

        g4_free(pixels);
    }

    return EXIT_SUCCESS;
}
