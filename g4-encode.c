/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <arpa/inet.h> // for htonl()

#include "g4-encode.h"
#include "g4-etables.h"

struct G4Encoder
{
    FILE *stream;
    uint32_t accumulator;
    int fill;
    int space;
#ifdef G4_CODER_DEBUG_OUTPUT
    long start_offset;
#endif
};

G4Encoder *g4_encoder_new(FILE *f)
{
    G4Encoder *e = (G4Encoder *) malloc(sizeof(G4Encoder));
    e->stream = f;
    e->accumulator = 0;
    e->fill = 0;
    e->space = sizeof(e->accumulator) * 8;
    #ifdef G4_CODER_DEBUG_OUTPUT
        e->start_offset = ftell(f);
    #endif
    return e;
}

void g4_encoder_free(G4Encoder *e)
{
    free(e);
}

#if 0
// append len bits from val.
// unused bits in val must be 0.
void g4_bitstream_write(G4Encoder *e, int val, int len)
{
    int free_space = 8 - e->fill;
    if (len < free_space)
    {
        e->accumulator |= val << (free_space - len);
        e->fill += len;
        return;
    }
    len -= free_space;
    e->accumulator |= val >> len;
    fputc(e->accumulator, e->stream);
    e->accumulator = 0;
    e->fill = 0;

    while (len >= 8)
    {
        len -= 8;
        fputc(val >> len, e->stream);
    }
    
    if (len)
    {
        e->accumulator = val << (8 - len);
        e->fill = len;
    }
}

void g4_bitstream_finish(G4Encoder *e)
{
    if (e->fill)
    {
        fputc(e->accumulator, e->stream);
        e->accumulator = 0;
        e->fill = 0;
    }
}
#else
// _____________   32-bit   ______________

void g4_bitstream_write(G4Encoder *e, int val, int len)
{
    e->space -= len;
    if (e->space >= 0)
    {
        e->accumulator += (uint32_t) val << e->space;
        return;
    }
    
    uint32_t a = htonl(e->accumulator + (val >> -e->space));
    fwrite(&a, 1, sizeof(a), e->stream);
    
    e->space += sizeof(e->accumulator) * 8;
    e->accumulator = (uint32_t) val << e->space;
}

void g4_bitstream_finish(G4Encoder *e)
{
    if (e->space < (int) sizeof(e->accumulator) * 8)
    {
        uint32_t a = htonl(e->accumulator);
        fwrite(&a, 1, sizeof(a), e->stream);
        e->accumulator = 0;
        e->space = sizeof(e->accumulator) * 8;
    }
}
#endif

static void encode_run_length(G4Encoder *e,
        int *small_codes, int *small_lengths, 
        int *big_codes, int *big_lengths,
        int run)
{
    while (run > G4_MAX_SMALL_VALUE)
    {
        int big_run = run;
        if (big_run > G4_MAX_TABLE_RUN)
            big_run = G4_MAX_TABLE_RUN;
        big_run /= G4_BIG_TABLE_STEP;
        g4_bitstream_write(e, big_codes[big_run], big_lengths[big_run]);
        run -= big_run * G4_BIG_TABLE_STEP;
    }
    g4_bitstream_write(e, small_codes[run], small_lengths[run]);
}

static void encode_white_run_length(G4Encoder *e, int run)
{
    encode_run_length(e,
            g4_small_white_code_values, g4_small_white_code_lengths, 
            g4_big_white_code_values, g4_big_white_code_lengths,
            run);
}

static void encode_black_run_length(G4Encoder *e, int run)
{
    encode_run_length(e,
            g4_small_black_code_values, g4_small_black_code_lengths, 
            g4_big_black_code_values, g4_big_black_code_lengths,
            run);
}

void g4_encode_line(G4Encoder *e, const int *current_line, const int *upper_line, int width)
{
    // This code closely follows g4_decode_line().
    int i = 0;
    int j = 1;

    int a = 0;
    int b = upper_line[0];
    int accumulated_passes = 0;

    #ifdef G4_CODER_DEBUG_OUTPUT
        printf("___________________\n");
    #endif
    while (1)
    {
        #ifdef G4_CODER_DEBUG_OUTPUT
            printf("main encoder loop start: a = %d, b = %d\n", a, b);
            printf("main encoder loop start: encoder offset %ld, fill %d\n", ftell(e->stream) - e->start_offset, e->fill);
        #endif
        int v = current_line[i] + a - b - accumulated_passes;
        if (v >= G4_MIN_V && v <= G4_MAX_V)
        {
            // Vertical
            #ifdef G4_CODER_DEBUG_OUTPUT
                printf("V%d, accumulated_passes = %d\n", v, accumulated_passes);
            #endif
            g4_bitstream_write(e, G4_V_CODE(v), G4_V_CODE_LEN(v));
            i++;
            accumulated_passes = 0;
            a = b + v;

            // color change
            if (j || b >= width)
                b -= upper_line[--j];
            else
                b += upper_line[j++];
        } // V
        else
        {
            int new_accumulated_passes = b < width ? accumulated_passes - a + b + upper_line[j]
                                                   : INT_MAX;
            if (new_accumulated_passes < current_line[i])
            {
                // Pass
                #ifdef G4_CODER_DEBUG_OUTPUT
                    printf("P\n");
                #endif
                g4_bitstream_write_PASS(e);
                b += upper_line[j++];
                a = b;
                b += upper_line[j++];
                accumulated_passes = new_accumulated_passes;
            } // P
            else
            {
                // Horizontal
                g4_bitstream_write_HORIZONTAL(e);
                int run1 = current_line[i++] - accumulated_passes;
                int run2 = a + run1 < width ? current_line[i++]
                                            : (i++, 0); // keeping parity but the run is 0
                a += run1 + run2;
                accumulated_passes = 0;
                #ifdef G4_CODER_DEBUG_OUTPUT
                    printf("H (%d, %d)\n", run1,
                                           run2);
                #endif
                if (i & 1)
                {
                    encode_black_run_length(e, run1);
                    encode_white_run_length(e, run2);
                }
                else
                {
                    encode_white_run_length(e, run1);
                    encode_black_run_length(e, run2);
                }
            } // H
        } // P or H branch

        // this is main loop again
        if (a >= width)
            break; // the only point of exit from the loop

        while (b <= a)
        {
            b += upper_line[j++];
            if (b == width)
                break;
            b += upper_line[j++];
        }
    } // main loop
} // g4_encode_line()
