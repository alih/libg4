/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <stdlib.h>

#include "g4-tiff.h"
#include "g4-easyapi.h"

void g4_free(unsigned char *image)
{
    free(image);
}

unsigned char *g4_load(const char *path,
                       int alignment,
                       int *out_width, 
                       int *out_height, 
                       int *out_stride)
{
    if (alignment < 0)
    {
        errno = EINVAL;
        return NULL;
    }

    G4Tiff *g4 = g4_open(path, "r");
    if (!g4)
    {
        perror(path);
        return NULL; 
    }

    int w = g4_width(g4);
    int h = g4_height(g4);

    if (w <= 0 || h <= 0)
    {
        fprintf(stderr, "Invalid image size (error: %s)\n", g4_error(g4));
        return NULL;
    }

    *out_width = w;
    *out_height = h;

    if (!alignment)
        alignment = 1;
    int stride = (w + 7) >> 3;
    stride += alignment - 1;
    stride -= stride % alignment;
    *out_stride = stride;

    unsigned char *image = (unsigned char *) malloc(stride * h);
    if (image)
    {
        G4Reader *r = g4_get_reader(g4);
        
        for (int i = 0; i < h; i++)
        {
            if (!g4_read_line(r, image + i * stride))
            {
                fprintf(stderr, "Error: %s\n", g4_reader_error(r));
                free(image);
                image = NULL;
                break;
            }
        }

        g4_reader_free(r);
    }

    g4_close(g4);
    return image;
}

bool g4_save(const char *path, 
            const unsigned char *image, 
            int width, 
            int height, 
            int stride) 
{
    G4Tiff *g4 = g4_open(path, "w");
    if (!g4)
    {
        perror(path);
        return false; 
    }
    
    g4_set_size(g4, width, height);
    G4Writer *w = g4_get_writer(g4);
    bool result = true;

    for (int i = 0; i < height; i++)
    {
        if (!g4_write_line(w, image + i * stride))
        {
            fprintf(stderr, "Error: %s\n", g4_writer_error(w));
            result = false;
            break;
        }
    }

    g4_writer_free(w);
    g4_close(g4);

    return result;
}
