/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef G4_DECODE_H
#define G4_DECODE_H

typedef struct G4Decoder G4Decoder;

G4Decoder *g4_decoder_new(const void *data, int length);
void g4_decoder_free(G4Decoder *);
const char *g4_decoder_error(G4Decoder *d);

void g4_decoder_set_strip_id(G4Decoder *, int id);

int g4_decode_line(G4Decoder *d,
                   int *current_line, 
                   int *upper_line, 
                   int width);

#endif
