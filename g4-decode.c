/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>  // for ntohl()

#include "g4-decode.h"
#include "g4-dtables.h"
#include "g4-constants.h"

struct G4Decoder
{
    const uint32_t *data32; // an aligned version of the data pointer
    const unsigned char *data; // the original data pointer
    int offset32; // offset in 32-bit words with respect to data32
    int end32;
    int offset;
    int end;
    uint32_t accum;
    int fill;
    int strip_id;

    const char *error;
};

static void load_byte(unsigned int *p_accum, int *p_fill, G4Decoder *d)
{
    int next_byte = d->data[d->offset++];
    // printf("getting next byte: %X at %d\n", next_byte, d->offset - 1);
    *p_accum += next_byte << (sizeof(*p_accum) * 8 - 8 - *p_fill);
    *p_fill += 8;
}

void g4_decoder_set_strip_id(G4Decoder *d, int id)
{
    d->strip_id = id;
}

G4Decoder *g4_decoder_new(const void *data, int length)
{
    if (!data)
        return NULL;
    G4Decoder *d = (G4Decoder *) malloc(sizeof(G4Decoder));
    d->data = (const unsigned char *) data;
    d->offset = 0;
    d->accum = 0;
    d->fill = 0;
    d->end = length;
    d->error = NULL;

    // preloading to align
    // (even if we won't use the 32-bit decoder, preloading doesn't hurt)
    enum { LOG_ALIGNMENT = 2,
           ALIGNMENT = 1 << LOG_ALIGNMENT,
           ALIGNMENT_MASK = ALIGNMENT - 1 };
    
// FIXME make a macro
#if defined(_ISOC11_SOURCE) || defined(__cplusplus)
    static_assert(ALIGNMENT <= sizeof(d->accum));
#else
    typedef int static_assert_we_can_safely_load_ALIGNMENT_bytes_into_accum
        [ALIGNMENT <= sizeof(d->accum) ? 1 : -1];
    (void) (static_assert_we_can_safely_load_ALIGNMENT_bytes_into_accum *) NULL;
#endif

    int to_preload = ALIGNMENT - ((intptr_t) data & ALIGNMENT_MASK);
    d->data32 = (uint32_t *) ((uintptr_t) data + to_preload);
    if (to_preload > length || length <= (int) sizeof(d->accum))
    {
        // load it full
        d->end32 = 0;
        to_preload = length;
    }
    else
    {
        d->end32 = (length - to_preload) >> LOG_ALIGNMENT;
    }

    for (int i = 0; i < to_preload; i++)
        load_byte(&d->accum, &d->fill, d);  

    d->offset32 = 0;

    return d;
}

void g4_decoder_free(G4Decoder *d)
{
    free(d);
}

const char *g4_decoder_error(G4Decoder *d)
{
    return d->error;
}

#ifdef G4_USE_32BIT_HUFFMAN // off by default, it's too slow; don't forget to regenerate the tables

/**
 * This function has lower level than g4_huffman_decode():
 * it doesn't deal with microterminals (table jumps).
 */
static int huffman_decode(unsigned int *p_accum,
                          int *p_fill,
                          G4Decoder *d,
                          const G4HuffmanTableEntry *table, 
                          int log_table_size)
{
    //printf("accum %d fill %d log_table_size %d\n", *p_accum, *p_fill, log_table_size);
    //printf("strip %d offset %d\n", d->strip_id,((unsigned char *) (d->data32 + d->offset32) - d->data)); 
    // we might get a partial table index here if fill < log_table_size
    unsigned table_index = *p_accum >> (sizeof(*p_accum) * 8 - log_table_size);
    
    // this might be the wrong entry if we don't have enough bits
    // but in this case, entry.length will be greater than fill
    G4HuffmanTableEntry entry = table[table_index];
    if (*p_fill >= entry.length)
    {
        // we got all the bits we need
        *p_accum <<= entry.length; 
        *p_fill -= entry.length;
        //printf("fast return %d\n", entry.terminal);
        return entry.terminal;
    }

    if (*p_fill >= log_table_size)
    {
        //printf("error return\n");
        return G4_ERROR;
    }

    // we need more bits
    int bits_used = *p_fill;
    int bits_needed = log_table_size - *p_fill;
    if (d->offset32 < d->end32)
    {
        *p_accum = ntohl(d->data32[d->offset32++]);
        *p_fill = sizeof(*p_accum) * 8;
    }
    else
    {
        //printf("switching to 8 bits\n");
        if (d->offset <= (int) sizeof(d->accum))
            d->offset = ((unsigned char *) (d->data32 + d->offset32) - d->data);
        while (*p_fill <= (int) sizeof(*p_accum) * 8 - 8)
        {
            // a long string of zeros will lead to error in G4 coding anyway
            if (d->offset >= d->end)
            {
                // printf("we're out of input\n");
                *p_fill = sizeof(*p_accum) * 8;
                break;
            }
            else
            {
                load_byte(p_accum, p_fill, d);
            }
        }
    }

    table_index += *p_accum >> (sizeof(*p_accum) * 8 - bits_needed);
    // this time we're shooting true
    entry = table[table_index];
    *p_accum <<= entry.length - bits_used; 
    *p_fill -= entry.length - bits_used;
    // printf("slow return %d, fill %d, entry.length %d, bits_used %d\n", entry.terminal, *p_fill, entry.length, bits_used);
    return entry.terminal;
}

#else
/**
 * The old decoder. Doesn't require htonl() and other crazy stuff 
 * like that uint8* -> uint32* cast. 
 */
static int huffman_decode(unsigned int *p_accum,
                           int *p_fill,
                           G4Decoder *d,
                           const G4HuffmanTableEntry *table, 
                           int log_table_size)
{
    if (*p_fill < log_table_size)
    {
        // I can think of a better way to do this on big-endian machines 
        // but I don't have a single one. Anyway, this is portable.
        while ((unsigned) *p_fill <= sizeof(*p_accum) * 8 - 8)
        {
            if (d->offset < d->end)
                load_byte(p_accum, p_fill, d);
            else
		*p_fill = sizeof(*p_accum) * 8;
        }
    }

    // printf("accum: %X, fill %d, log_table_size %d\n", d->accum, d->fill, log_table_size);
    unsigned table_index = *p_accum >> (sizeof(*p_accum) * 8 - log_table_size);
    // printf("table_index: x%X\n", table_index);
    G4HuffmanTableEntry entry = table[table_index];
    *p_accum <<= entry.length;
    *p_fill -= entry.length;
    // printf("after-read accum: %X, fill %d, elength %d, eterm %d\n", d->accum, d->fill, entry.length, entry.terminal);
    assert(entry.length >= 0 || entry.terminal == G4_ERROR);

    //printf("huffman8: return %d at offset %d, strip %d\n", entry.terminal, d->offset, d->strip_id); 
    return entry.terminal;
}
#endif


/**
 * Huffman decoder.
 *
 *   p_accum and p_fill are optimizations.
 *   They point to local copies of d->accum and d->fill.
 */
static int g4_huffman_decode(unsigned int *p_accum,
                             int *p_fill,
                             G4Decoder *d,
                             const G4HuffmanTableEntry *table, 
                             int log_table_size)
{
    int t = huffman_decode(p_accum, p_fill, d, table, log_table_size);

    if (t >= G4_MIN_V)
    {
        // printf("g4_huffman_decode(): returns %d\n", t);
        return t;
    }

    // we got a microterminal
    switch (t)
    {
        case G4_JUMP_BLACK_LONG: 
            return g4_huffman_decode(p_accum, p_fill, d, g4_black_long_table, G4_BLACK_LONG_TABLE_LOG_SIZE);
        case G4_JUMP_HUGE_RUNS: 
            return g4_huffman_decode(p_accum, p_fill, d, g4_huge_run_table, G4_HUGE_RUN_TABLE_LOG_SIZE);
        case G4_JUMP_EOFB:
            return G4_EOFB; // don't care
        default:
            d->error = "Unknown terminal";
            return G4_ERROR;
    }
}

static int decode_run_length(unsigned int *p_accum, int *p_fill, G4Decoder *d, G4HuffmanTableEntry *table, int log_table_size)
{
    int result = 0;
    int c;
    do
    {
        c = g4_huffman_decode(p_accum, p_fill, d, table, log_table_size);
        if (c == G4_ERROR)
            return G4_ERROR;
        result += c;
    } while (c > G4_MAX_SMALL_VALUE);
    return result;
}

int g4_decode_line(G4Decoder *d,
                   int *current_line, 
                   int *upper_line, 
                   int width)
{
    unsigned int accum = d->accum;
    int fill = d->fill;
    int i = 0;
    int j = 1;

    int a = 0;
    int b = upper_line[0]; // upper line cursor, partial sum up to j
    int accumulated_passes = 0;

    #ifdef G4_CODER_DEBUG_OUTPUT
        printf("___________________\n");
    #endif
    // invariant: a == sum(current_line[0:i]) + accumulated_passes
    while (1)
    {
        #ifdef G4_CODER_DEBUG_OUTPUT
            printf("main decoder loop start: a = %d, b = %d\n", a, b);
            printf("main decoder loop start: decoder offset %d\n", d->offset);
        #endif
        int op = g4_huffman_decode(&accum, &fill, d, g4_main_table, G4_MAIN_TABLE_LOG_SIZE);
        if (op >= G4_MIN_V && op <= G4_MAX_V)
        {
            // Vertical
            #ifdef G4_CODER_DEBUG_OUTPUT
                printf("V%d, accumulated_passes = %d\n", op, accumulated_passes);
            #endif
            current_line[i++] = b + op - a + accumulated_passes;
            //printf("--> %d\n", current_line[i-1]);
            accumulated_passes = 0;
            // invariant restored:
            //    sum(current_line[0:i_new]) + accumulated_passes_new = 
            //
            //       // accumulated_passes_new = 0
            //    
            //    = sum(current_line[0:i_old]) + b + op
            //           - a_old + accumulated_passes_old =
            //    
            //       // - a_old = -sum(current_line[0:i_old] - accumulated_passes_old
            //
            //    = b + op
            a = b + op;

            // color change
            if (j || b >= width)
                b -= upper_line[--j];
            else
                b += upper_line[j++];
            
        }
        else switch (op)
        {
            case G4_HORIZONTAL:
            {
                G4HuffmanTableEntry *table1, *table2;
                int log_table1_size, log_table2_size;
                if (i & 1)
                {
                    table1 = g4_black_table;
                    table2 = g4_white_table;
                    log_table1_size = G4_BLACK_TABLE_LOG_SIZE;
                    log_table2_size = G4_WHITE_TABLE_LOG_SIZE;
                }
                else
                {
                    table1 = g4_white_table;
                    table2 = g4_black_table;
                    log_table1_size = G4_WHITE_TABLE_LOG_SIZE;
                    log_table2_size = G4_BLACK_TABLE_LOG_SIZE;
                }
                int run1 = decode_run_length(&accum, &fill, d, table1, log_table1_size);
                if (run1 == G4_ERROR)
                    return 0;
                int run2 = decode_run_length(&accum, &fill, d, table2, log_table2_size);
                if (run2 == G4_ERROR)
                    return 0;

                // invariant: a_old == sum(current_line[0:i_old]) + accumulated_passes_old
                current_line[i++] = run1 + accumulated_passes;
                current_line[i++] = run2;
                // sum(current_line[0:i_new]) == sum(current_line[0:i_old]) + 
                //                             + run1 + run2 + accumulated_passes_old
                a += run1 + run2;
                // a_new = a_old + run1 + run2 = 
                //      sum(current_line[0:i_old]) + accumulated_passes_old + run1 + run2 =
                //      sum(current_line[0:i_new])
                accumulated_passes = 0; // invariant restored
                #ifdef G4_CODER_DEBUG_OUTPUT
                    printf("H (%d, %d)\n", current_line[i - 2],
                                           current_line[i - 1]);
                #endif
            }
            break;

            case G4_PASS:
                #ifdef G4_CODER_DEBUG_OUTPUT
                    printf("P\n");
                #endif
                b += upper_line[j++];
                accumulated_passes += b - a;
                a = b;
                b += upper_line[j++];
            break;

            case G4_EOFB:
                d->error = "Premature end of block met";
                return 0;
            break;

            case G4_EXTENSION:
                d->error = "G4 extensions are not supported";
                return 0;
            break;

            case G4_ERROR:
            default:
                d->error = "Huffman decoding error";
                return 0;
        } // big switch

        if (a > width)
        {
            d->error = "A G4 line is longer than the image width";
            return 0;
        }
        if (a >= width)
            break; // the only point of exit from the loop

        // printf("main loop pre-advance: a = %d, b = %d, c = %d\n", a, b, c);
        
        // the advance loop
        while (b <= a)
        {
            b += upper_line[j++];
            // we don't check for (b == width) here for speed
            // but we need the advance loop guard for that to work
            // because we don't want to catch a random value here
            // BTW, an empty white line doesn't need the guard
            b += upper_line[j++];
        }
        if (b > width)
            b -= upper_line[--j];
    } // main loop

    if (accumulated_passes)
        current_line[i++] = accumulated_passes;
    if (i & 1)
        current_line[i++] = 0;
    current_line[i] = width; // the advance loop guard

    /*printf("%d runs:\n", i);
    for (j = 0; j < i; j++)
        printf(" %d", current_line[j]);
    printf("\n"); */

    d->accum = accum;
    d->fill = fill;

    return i;
}
