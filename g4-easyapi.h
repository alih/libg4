/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef G4_EASYAPI_H
#define G4_EASYAPI_H

#include <stdbool.h>

/**
 * Load a G4-compressed bitmap from the given TIFF file.
 *
 * \param      alignment    The required alignment for the stride.
 *                          0 means don't care.
 *                          (This assumes that malloc has a bigger alignment)
 *
 * \param[out] out_width    The place to store the image width.
 * \param[out] out_height   The place to store the image height.
 * \param[out] out_stride   The place to store the stride.
 *
 * \returns                 An array of bits packed into bytes,
 *                          from the MSB to the LSB (PBM-style).
 *                          Returns NULL in case of error and sets errno.
 */
unsigned char *g4_load(const char *path, 
                       int alignment,
                       int *out_width, 
                       int *out_height,
                       int *out_stride);

/**
 * Free an image obtained from g4_load().
 */
void g4_free(unsigned char *image);

bool g4_save(const char *path, 
             const unsigned char *image, 
             int w, int h, int stride); 

#endif
