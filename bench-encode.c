#if 0

gcc -std=c99 -O3 -DNDEBUG -I. bench-encode.c -I. -Igenerated g4-*.c generated/g4-*.c -ltiff -o bench-encode
exit

#endif 

/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <tiffio.h>
#include "g4.h"

static void write_tiff(const char *path, unsigned char *image, int width, int height, int stride)
{
    TIFF *tiff = TIFFOpen(path, "w");
    TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, width);
    TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, height);
    TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, 1);
    TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL, 1);
    TIFFSetField(tiff, TIFFTAG_ROWSPERSTRIP, height);
    TIFFSetField(tiff, TIFFTAG_COMPRESSION, COMPRESSION_CCITTFAX4);

    for (int i = 0; i < height; i++)
    {
        TIFFWriteScanline(tiff, image + stride * i, i, 0);
    }

    TIFFClose(tiff);
}

int main(int argc, const char *const *argv)
{
    if (argc == 1)
    {
        fprintf(stderr, "Usage:\n");
        fprintf(stderr, "    %s input1.tif input2.tif:\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int n_iter = 100;

    double total_g4_time = 0;
    double total_g4_rle_time = 0;
    double total_libtiff_time = 0;

    for (int a = 1; a < argc; a++)
    {
        clock_t begin, end;
        int width, height, stride;
        unsigned char *image = g4_load(argv[a], 1, &width, &height, &stride);

        printf("------- loaded: %s\n", argv[a]);
        begin = clock();
        for (int i = 0; i < n_iter; i++)
            g4_save("/dev/null", image, width, height, stride);
        end = clock();
        double g4_time = (double) (end - begin) / CLOCKS_PER_SEC;
        printf("libg4 time: %lf\n", g4_time);
        total_g4_time += g4_time;

        int *offsets = malloc((height + 1) * sizeof(int));
        int *runs = malloc((width + 10) * height * sizeof(int));
        offsets[0] = 0;
        for (int i = 0; i < height; i++)
        {
            int k = g4_rle_extract(runs + offsets[i], image + i * stride, width);
            offsets[i + 1] = offsets[i] + k;
        }

        begin = clock();
        for (int i = 0; i < n_iter; i++)
        {
            G4Tiff *g4 = g4_open("/dev/null", "w");
            g4_set_size(g4, width, height);
            G4Writer *w = g4_get_writer(g4);

            for (int y = 0; y < height; y++)
            {
                memcpy(g4_writer_rle(w), runs + offsets[y], (offsets[y + 1] - offsets[y]) * sizeof(int));
                g4_writer_write_rle_line(w);
            }

            g4_writer_free(w);
            g4_close(g4);
        }
        end = clock();
        double g4_rle_time = (double) (end - begin) / CLOCKS_PER_SEC;
        printf("libg4 RLE time: %lf\n", g4_rle_time);
        total_g4_rle_time += g4_rle_time;

        begin = clock();
        for (int i = 0; i < n_iter; i++)
            write_tiff("/dev/null", image, width, height, stride);
        end = clock();
        double libtiff_time = (double) (end - begin) / CLOCKS_PER_SEC;
        printf("libtiff time: %lf\n", libtiff_time);
        total_libtiff_time += libtiff_time;

        g4_free(image);
    }
    printf("========== TOTAL ==========\n");
    printf("libg4 time: %lf\n", total_g4_time);
    printf("libg4 (from RLE) time: %lf\n", total_g4_rle_time);
    printf("libtiff time: %lf\n", total_libtiff_time);

    return EXIT_SUCCESS;
}
