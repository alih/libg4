/*
 * libg4 - a library for reading and writing CCITT Group 4 compressed TIFFs
 * Copyright (C) 2015  Ilya Mezhirov
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "g4-tiff.h"

static void skip_comment_line(FILE *file)
{
    while (1) switch(fgetc(file))
    {
        case EOF: case '\r': case '\n':
            return;
    }
}


static void skip_whitespace_and_comments(FILE *file)
{
    int c = fgetc(file);
    while(1) switch(c)
    {
        case '#':
            skip_comment_line(file);
            /* fall through */
        case ' ': case '\t': case '\r': case '\n':
            c = fgetc(file);
        break;
        case EOF:
            return;
        default:
            ungetc(c, file);
            return;
    }
}

static bool read_pbm_header(FILE *f, int *out_width, int *out_height)
{
    if (getc(f) != 'P')
    {
        fprintf(stderr, "Error: PBM file doesn't start with 'P'\n");
        return false;
    }
    
    char type = getc(f);
    if (type != '4')
    {
        fprintf(stderr, "Error: Only (raw) PBM supported\n");
        return false;
    }
    skip_whitespace_and_comments(f);
    
    if (fscanf(f, "%d %d", out_width, out_height) != 2)
    {
        fprintf(stderr, "Error: Unable to read width and height\n");
        return false;
    }

    if (*out_width <= 0 || *out_height <= 0)
    {
        fprintf(stderr, "Error: PBM size must be greater than 0\n");
        return false;
    }

    switch(fgetc(f))
    {
        case ' ': case '\t': case '\r': case '\n':
            break;
        default:
            fprintf(stderr, "corrupted PGM, current offset is %ld\n", ftell(f));
            return false;
    }

    return true;
}

static void pbm_to_g4(const char *path, FILE *f, G4Tiff *g4)
{
    int width, height;
    if (!read_pbm_header(f, &width, &height))
        exit(EXIT_FAILURE);

    g4_set_size(g4, width, height);
    G4Writer *w = g4_get_writer(g4);

    int stride = (width + 7) >> 3;
    unsigned char buf[stride];

    for (int i = 0; i < height; i++)
    {
        if (fread(buf, 1, sizeof(buf), f) != sizeof(buf))
        {
            perror(path);
            exit(EXIT_FAILURE);
        }
        if (!g4_write_line(w, buf))
        {
            fprintf(stderr, "Error: %s\n", g4_writer_error(w));
            exit(EXIT_FAILURE);
            break;
        }
    }

    g4_writer_free(w);
}

static void run_pbm_to_g4(const char *input_path, const char *output_path)
{
    FILE *f = strcmp(input_path, "-") ? fopen(input_path, "rb") 
                                      : stdin;
    if (!f)
    {
        perror(input_path);
        exit(EXIT_FAILURE);
    }
    G4Tiff *g4 = g4_open(output_path, "w");
    if (!g4)
    {
        perror(output_path);
        if (f != stdin)
            fclose(f);
        exit(EXIT_FAILURE);
    }

    pbm_to_g4(input_path, f, g4);
    
    g4_close(g4);
    if (f != stdin)
        fclose(f);
}

int main(int argc, const char *const *argv)
{
    if (argc < 3 || argc > 4 
        || !strcmp(argv[1], "-h")
        || !strcmp(argv[1], "--help"))
    {
        fprintf(stderr, "%s - convert G4 TIFF images to PBM\n", argv[0]);
        fprintf(stderr, "Usage:\n");
        fprintf(stderr, "    %s input.pbm output.tif\n", argv[0]);
        //fprintf(stderr, "Page numbering starts from 1.\n");
        //fprintf(stderr, "The default behavior is to convert all pages.\n");
        fprintf(stderr, "You can specify `-' instead of `input.pbm' for stdin.\n");
        fprintf(stderr, "TIFF files, however, can't be written to stdout.\n");
        exit(EXIT_FAILURE);
    }

    const char *input_path = argv[1];
    const char *output_path = argv[2];
    int page = 0; // 0 means all
    if (argc > 3)
    {
        page = atoi(argv[3]);
        if (page <= 0)
        {
            fprintf(stderr, "Invalid page number: %s\n", argv[3]);
            exit(EXIT_FAILURE);
        }
    }

    run_pbm_to_g4(input_path, output_path/*, page*/);
    
    return EXIT_SUCCESS;
}
