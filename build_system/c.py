from os.path import *
from build_system.base import *

obj_extension = '.o'

class CConfig(Config):
    def write_rules(self, ninja, mod_name=None):
        ninja.newline()
        ninja.comment("Rules for the config `%s'" % self.name())
        self.write_compile_rule(ninja, mod_name=mod_name)
        self.write_link_rule(ninja, mod_name=mod_name)
        self.write_stlib_rule(ninja, mod_name=mod_name)

    def write_compile_rule(self, ninja, mod_name=None):
        config_cflags = ' '.join(self.cflags())
        if mod_name != None:
            config_cflags += ' ${%s}' % decorate_mod('cflags', mod_name)
        ninja.rule(name = self.decorate('compile', mod_name),
                   command = '$cc $cflags %s -MD -MF $out.d -c $in -o $out' % config_cflags,
                   deps = 'gcc',
                   depfile = '$out.d')

    def write_link_rule(self, ninja, mod_name=None):
        config_ldflags = ' '.join(self.ldflags())
        if mod_name != None:
            config_ldflags += ' ${%s}' % decorate_mod('ldflags', mod_name) # TODO: dedup
        ninja.rule(name = self.decorate('link', mod_name),
                   command = '$cc $ldflags $in -o $out %s' % config_ldflags)

    def write_stlib_rule(self, ninja, mod_name=None):
        ninja.rule(name = self.decorate('stlib', mod_name),
                   command = 'ar cr $out $in && ranlib $out')
        

class CReleaseConfig(CConfig):
    def name(self):
        return 'release'
    def cflags(self):
        return ['-O3', '-DNDEBUG']
    def ldflags(self):
        return ['-O3']

class CDebugConfig(CConfig):
    def name(self):
        return 'debug'
    def cflags(self):
        return ['-g']
    def ldflags(self):
        return ['-g']

class CCoverageConfig(CConfig):
    def name(self):
        return 'coverage'
    def cflags(self):
        return ['-g', '--coverage']
    def ldflags(self):
        return ['-g', '--coverage']



def get_default_c_configs():
    result = [CReleaseConfig(),
              CDebugConfig(),
              CCoverageConfig()]
    result[0].is_default = True
    return result

def flatten(a):
    if not isinstance(a, list) and not isinstance(a, tuple):
        return a
    result = []
    for i in a:
        f = flatten(i)
        if isinstance(f, list) or isinstance(f, tuple):
            result += f
        else:
            result.append(f)
    return result

class C(Tool):
    def __init__(self, 
                 ninja, 
                 srcdir, 
                 cflags=None, 
                 ldflags=None,
                 configs=None, 
                 cc='gcc', 
                 parent=None, 
                 mod_name=None, 
                 compiledeps=None):
        self.ninja = ninja
        self.cflags = cflags or (parent and parent.cflags) or []
        self.ldflags = ldflags or (parent and parent.cflags) or []
        self.srcdir = srcdir
        self.cc = cc
        self.parent = parent

        self.compiledeps = compiledeps or (parent and parent.compiledeps)
        #if compiledeps is None and parent is not None:
        #    self.compiledeps = parent.compiledeps
        #else:
        #    self.compiledeps = compiledeps

        self.mod_name = mod_name
        if configs is None:
            configs = get_default_c_configs()
        self.configs = configs

        if self.parent is None:
            self.write_variables()
        else:
            self.write_mod_variables()
        for conf in self.configs:
            conf.write_rules(self.ninja, self.mod_name)

    def mod(self, name, cflags=None, ldflags=None, compiledeps=None):
        return C(ninja = self.ninja, 
                cflags = cflags, 
                ldflags = ldflags,
                srcdir = self.srcdir, 
                configs = self.configs, 
                cc = self.cc, 
                parent = self,
                mod_name = name,
                compiledeps=compiledeps)


    def write_variables(self):
        self.ninja.newline()
        self.ninja.comment('C-related variables')
        self.ninja.variable('cc', self.cc)
        self.ninja.variable('cflags', ' '.join(self.cflags))
        self.ninja.variable('ldflags', ' '.join(self.ldflags))

    def write_mod_variables(self):
        self.ninja.newline()
        self.ninja.comment('variables of mod ' + self.mod_name)
        self.ninja.variable(decorate_mod('cflags', self.mod_name), ' '.join(self.cflags))
        self.ninja.variable(decorate_mod('ldflags', self.mod_name), ' '.join(self.ldflags))

    def _compile_target(self, src):
        assert isinstance(src, Target)
        result = Target(src.path + obj_extension)
        for conf in self.configs:
            self.ninja.build(inputs = src.path_in_config(conf),
                             outputs = result.path_in_config(conf), 
                             rule = conf.get_rule('compile'))
        return result

    def source_path(self, src):
        return normpath(join(self.srcdir, src))
    
    def _compile_source(self, src):
        assert isinstance(src, str)
        result = Target(src + obj_extension)
        for conf in self.configs:
            self.ninja.build(inputs = self.source_path(src),
                             outputs = result.path_in_config(conf),
                             rule = decorate_mod(conf.get_rule('compile'), self.mod_name),
# implicit deps are taken from the default config! FIXME this is stupid
                             implicit = None if self.compiledeps is None
                                             else [join('$builddir', d.path) for d in self.compiledeps])
        return result

    def _compile_one(self, src):
        if isinstance(src, Target):
            return self._compile_target(src)
        else:
            return self._compile_source(src)

    def compile(self, *src):
        result = [self._compile_one(i) for i in flatten(src)]
        if len(result) == 1:
            return result[0]
        return result

    def link(self, out, *args):
        if not isinstance(out, Target):
            out = Target(out)
        for conf in self.configs:
            self.ninja.build(inputs = [t.path_in_config(conf) for t in flatten(args)],
                             outputs = out.path_in_config(conf), 
                             rule = decorate_mod(conf.get_rule('link'), self.mod_name))
        return out

    def stlib(self, out, *args):
        result = Target('lib%s.a' % out)
        for conf in self.configs:
            self.ninja.build(inputs = [t.path_in_config(conf) for t in flatten(args)],
                             outputs = result.path_in_config(conf), 
                             rule = decorate_mod(conf.get_rule('stlib'), self.mod_name))
        return result
