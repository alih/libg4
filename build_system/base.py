#! /usr/bin/env python3

"""
This is the base of the (nameless so far) build system.
"""

from os.path import join

class Tool:
    pass

class Target:
    def __init__(self, path):
        self.path = path
    def path_in_config(self, conf):
        if conf.is_default:
            return join('$builddir', self.path)
        return join('$builddir', conf.name(), self.path)

def decorate_mod(name, mod=None):
    if mod != None:
        return '%s__%s' % (name, mod)
    else:
        return name

class Config:
    """
    A Config is a manner of compilation: release, debug or something else.
    """
    is_default = False

    def write_rules(self, ninja):
        pass

    def get_rule(self, name):
        return self.decorate(name)    

    def decorate(self, name, mod=None):
        if self.is_default:
            return decorate_mod(name, mod)
        return decorate_mod('%s_%s' % (self.name(), name), mod)
